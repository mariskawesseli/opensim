import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('TkAgg')


def plotKinematics(file_to_read, x_value='time', pf=1):
    """ function to read knee kinematics from .sto file and plot 6 degrees of freedom"""
    # fig, ax = plt.subplots(2, 3)
    # columns containing tibiofemoral kinematics
    tf_cols = ['knee_flex_r', 'knee_add_r', 'knee_rot_r', 'knee_tx_r', 'knee_ty_r', 'knee_tz_r']
    # columns containing patellofemoral kinematics
    pf_cols = ['pf_flex_r', 'pf_rot_r', 'pf_tilt_r', 'pf_tx_r', 'pf_ty_r', 'pf_tz_r']
    # read data from sto file dependent on header
    try:
        comak_kin = pd.read_csv(file_to_read, sep='\t', header=8)
        tf_kin = comak_kin.loc[:, tf_cols]
        linestyle = 'solid'
    except KeyError:
        comak_kin = pd.read_csv(file_to_read, sep='\t', header=5)
        tf_kin = comak_kin.loc[:, tf_cols]
        linestyle = 'dashed'

    # add patellofemoral kinematics
    if pf == 1:
        pf_kin = comak_kin.loc[:, pf_cols]
        tf_kin = pd.concat([tf_kin, pf_kin], axis=1)
        tf_cols = tf_cols + pf_cols
        tf_kin[tf_cols[9:12]] *= 1000  # convert translations to mm

    tf_kin[tf_cols[3:6]] *= 1000  # convert translations to mm
    # add x-axis data
    tf_kin[x_value+'[]'] = comak_kin.loc[:, x_value]
    # plot
    # if 'prescribed' in file_to_read:
    #     tf_kin['knee_add_r'] *= -1
    #     tf_kin['knee_rot_r'] *= -1
    #     tf_kin['knee_tz_r'] *= -1
    #     tf_kin['pf_rot_r'] *= -1
    #     tf_kin['pf_tilt_r'] *= -1
    #     tf_kin['pf_tz_r'] *= -1

    tf_kin.plot(subplots=True, ax=ax, x=x_value+'[]', legend=0, title=tf_cols, linewidth='0.5', linestyle=linestyle)

    return tf_kin


fig, ax = plt.subplots(4, 3)
main_dir = [r'C:\Users\mariskawesseli\Documents\OpenSim\4DCT\Patient001\results']
comak_kin_results = os.path.join(main_dir[0],  'knee_flex_values.sto')
plotKinematics(comak_kin_results)

main_dir = r'C:\Users\mariskawesseli\Documents\OpenSim\4DCT\Patient001'
file = r'prescribed_coordinates_R1_patella.sto'
ct_kinematics_file = os.path.join(main_dir, file)
plotKinematics(ct_kinematics_file)

fig, ax = plt.subplots(4, 3)
main_dir = [r'C:\Users\mariskawesseli\Documents\OpenSim\4DCT\Patient001\results']
comak_kin_results = os.path.join(main_dir[0],  'knee_flex_values.sto')
tf_kin = plotKinematics(comak_kin_results, x_value='knee_flex_r')

main_dir = r'C:\Users\mariskawesseli\Documents\OpenSim\4DCT\Patient001'
file = r'prescribed_coordinates_R1_patella.sto'
ct_kinematics_file = os.path.join(main_dir, file)
plotKinematics(ct_kinematics_file, x_value='knee_flex_r')
