import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd

os.environ['PATH'] = r"C:\OpenSim 4.3\bin" + os.pathsep + os.environ['PATH']
import opensim
import matplotlib
matplotlib.use('TkAgg')

subjects = ['S0']  # [9,13,19,23,26,29,32,35,37,41]
sides = ['L']
segments = ['femur','tibia', 'fibula']
opensim_meshes = ['smith2019-L-femur-bone_remesh.stl','smith2019-L-tibia-bone_remesh.stl',
                  'smith2019-L-fibula-bone_remesh.stl']

opensim_geometry_folder = r'C:\opensim-jam\jam-resources\jam-resources-main\models\knee_healthy\smith2019\Geometry'
input_file_folder = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output'
gen_model = r'C:\opensim-jam\opensim-jam-release\examples\walkingS0L\S0L_lig_nocontact2.osim'  # generic scaled model without contact

data_folder = r"C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim"
cadaver_folder = r'C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData'

lig_femur = ["PCLpm1", "PCLpm2", "PCLpm3", "PCLpm4", "PCLpm5", "PCLal1", "PCLal2", "PCLal3", "PCLal4", "PCLal5",
             "MCLs1", "MCLs2", "MCLs3", "MCLs4", "MCLs5", "MCLs6", "MCLd1", "MCLd2", "MCLd3", "MCLd4", "MCLd5", "MCLp1",
             "MCLp2", "MCLp3","MCLp4", "MCLp5", "ACLpl1", "ACLpl2", "ACLpl3", "ACLpl4", "ACLpl5", "ACLpl6", "ACLam1",
             "ACLam2", "ACLam3", "ACLam4", "ACLam5", "ACLam6", "LCL1", "LCL2", "LCL3", "LCL4"]
lig_tibia = ["PCLpm1", "PCLpm2", "PCLpm3", "PCLpm4", "PCLpm5", "PCLal1", "PCLal2", "PCLal3", "PCLal4", "PCLal5",
             "MCLd1", "MCLd2", "MCLd3", "MCLd4", "MCLd5", "MCLp1", "MCLp2", "MCLp3", "MCLp4", "MCLp5", "ACLpl1",
             "ACLpl2", "ACLpl3", "ACLpl4", "ACLpl5", "ACLpl6", "ACLam1", "ACLam2", "ACLam3", "ACLam4", "ACLam5",
             "ACLam6"]
lig_fibula = ["LCL1", "LCL2", "LCL3", "LCL4"]

cf = 0
kin = 0
lig_forces = 1
kin_f = 0

if kin == 1:
    # bla=[]
    failed = []
    fail_sim_err = []
    err = []
    non_fail = []
    fig, ax = plt.subplots(2, 3)
    main_dir = [r'C:\opensim-jam\opensim-jam-release\examples\walkingS0L']
    tf_cols = ['knee_flex_l','knee_add_l','knee_rot_l','knee_tx_l','knee_ty_l','knee_tz_l']

    # results subject-specific ligament position
    # comak_kin_results = os.path.join(main_dir[0], 'results2lig', 'comak', 'walking_values.sto')
    # comak_kin = pd.read_csv(comak_kin_results, sep='\t', header=8)
    # time = comak_kin.loc[:, 'time']
    # stance = time[time == 6.58].index[0]
    # tf_kin = comak_kin.loc[0:stance, tf_cols]
    # tf_kin[tf_cols[3:]] = tf_kin[tf_cols[3:]] * 1000  # convert to mm
    # tf_kin.plot(subplots=True, ax=ax, legend=0, title=tf_cols, linewidth='4')

    # results generic model
    comak_kin_results = os.path.join(main_dir[0], 'results', 'comak', 'walking_values.sto')
    comak_kin = pd.read_csv(comak_kin_results, sep='\t', header=8)
    time = comak_kin.loc[:, 'time']
    stance = time[time == 6.58].index[0]
    tf_kin = comak_kin.loc[0:stance, tf_cols]
    tf_kin[tf_cols[3:]] = tf_kin[tf_cols[3:]] * 1000  # convert to mm
    tf_kin['Stance phase {%]'] = np.arange(0, 100, 100 / len(tf_kin))
    tf_kin.plot(subplots=True, ax=ax, legend=0, title=tf_cols, linewidth='4', linestyle='dashed', x='Stance phase {%]')
    # ax[0, 0].legend(["Personalized", "Generic"])

    for sample in range(0,100):
        for subj_ind, subject in enumerate(subjects):

            comak_kin_results = os.path.join(main_dir[0], 'results2lig_' + str(sample), 'comak', 'walking_values.sto')
            try:
                comak_kin = pd.read_csv(comak_kin_results, sep='\t', header=8)
                time = comak_kin.loc[:, 'time']
                stance = time[time == 6.58].index[0]
                tf_kin = comak_kin.loc[0:stance, tf_cols]
                tf_kin[tf_cols[3:]] = tf_kin[tf_cols[3:]]*1000  # convert to mm
                err.append(abs(tf_kin[tf_cols[:]].diff()/tf_kin[tf_cols[:]].max()).max().max())
                if abs(tf_kin[tf_cols[:]].diff()/tf_kin[tf_cols[:]].max()).max().max()>1000:
                    fail_sim_err.append([sample, abs(tf_kin[tf_cols[:]].diff()/tf_kin[tf_cols[:]].max()).max().max()])
                    # tf_kin.plot(subplots=True, legend=0, title=tf_cols, linewidth='0.5', alpha=0.5)
                else:
                    non_fail.append([sample, abs(tf_kin[tf_cols[:]].pct_change()).max().max()])
                    tf_kin['Stance phase {%]'] = np.arange(0, 100, 100 / len(tf_kin))
                    tf_kin.plot(subplots=True, ax=ax, legend=0, title=tf_cols, linewidth='1', alpha=0.2, x='Stance phase {%]')
                    if sample == 0:
                        ax[0, 0].legend(["Generic", "Personalized"])
                # bla.append(tf_kin)
            except Exception as e:
                failed.append([sample, e])
                pass

    # ax[0,1].set_ylim([-5, 5])
    # ax[0,2].set_ylim([-15, 5])
    # ax[1,2].set_ylim([-5, 10])


"""Contact Forces"""
if cf == 1:
    failed = []
    fail_sim_err = []
    err = []
    non_fail = []
    fig, ax = plt.subplots(1, 3)
    tf_cols = ['tf_contact.casting.total.contact_force_x','tf_contact.casting.total.contact_force_y','tf_contact.casting.total.contact_force_z']
    main_dir = [r'C:\opensim-jam\opensim-jam-release\examples\walkingS0L']
    # results subject-specific ligament position
    # comak_kin_results = os.path.join(main_dir[0], 'results2lig', 'joint-mechanics', 'walking_ForceReporter_forces.sto')
    # comak_kin = pd.read_csv(comak_kin_results, sep='\t', header=10)
    # time = comak_kin.loc[:, 'time']
    # stance = time[round(time, 2) == 6.58].index[0]
    # tf_kin = comak_kin.loc[0:stance, tf_cols]
    # tf_kin['time'] = time
    # tf_kin.plot(subplots=True, ax=ax, legend=0, title=tf_cols, linewidth='4', x='time')

    # results generic model
    comak_kin_results = os.path.join(main_dir[0], 'results', 'joint-mechanics', 'walking_ForceReporter_forces.sto')
    comak_kin = pd.read_csv(comak_kin_results, sep='\t', header=10)
    time = comak_kin.loc[:, 'time']
    stance = time[round(time, 2) == 6.58].index[0]
    tf_kin = comak_kin.loc[0:stance, tf_cols]
    tf_kin['time'] = time
    tf_kin.plot(subplots=True, ax=ax, legend=0, title=tf_cols, linewidth='4', linestyle='dashed', x='time')
    # ax[0].legend(["Personalized", "Generic"])

    for sample in range(0,100):
        for subj_ind, subject in enumerate(subjects):
            comak_kin_results = os.path.join(main_dir[0], 'results2lig_' + str(sample), 'comak', '3DGaitModel2392_ForceReporter_forces.sto')
            try:
                comak_kin = pd.read_csv(comak_kin_results, sep='\t', header=10)
                time = comak_kin.loc[:, 'time']
                stance = time[time == 6.58].index[0]
                tf_kin = comak_kin.loc[0:stance, tf_cols]
                # tf_kin[tf_cols[3:]] = tf_kin[tf_cols[3:]]*1000  # convert to mm
                err.append(abs(tf_kin[tf_cols[:]].diff()/tf_kin[tf_cols[:]].max()).max().max())
                if abs(tf_kin[tf_cols[:]].diff()/tf_kin[tf_cols[:]].max()).max().max()>1000:
                    fail_sim_err.append([sample, abs(tf_kin[tf_cols[:]].diff()/tf_kin[tf_cols[:]].max()).max().max()])
                    # tf_kin.plot(subplots=True, legend=0, title=tf_cols, linewidth='0.5', alpha=0.5)
                else:
                    non_fail.append([sample, abs(tf_kin[tf_cols[:]].pct_change()).max().max()])
                    tf_kin['time'] = time
                    tf_kin.plot(subplots=True, ax=ax, legend=0, title=tf_cols, linewidth='1', alpha=0.2, x='time')
                    if sample == 0:
                        ax[0, 0].legend(["Generic", "Personalized"])
                # bla.append(tf_kin)
            except Exception as e:
                failed.append([sample, e])
                pass


""" Ligament Forces"""
if lig_forces == 1:
    failed = []
    fail_sim_err = []
    err = []
    non_fail = []
    fig, ax = plt.subplots(2, 5)
    main_dir = [r'C:\opensim-jam\opensim-jam-release\examples\walkingS0L']
    tf_cols = [s + '.force_total' for s in lig_femur]
    # tf_kin_sum = pd.DataFrame()

    # results subject-specific ligament position
    # comak_kin_results = os.path.join(main_dir[0], 'results2lig', 'joint-mechanics', 'walking_ForceReporter_forces.sto')
    # comak_kin = pd.read_csv(comak_kin_results, sep='\t', header=10)
    # time = comak_kin.loc[:, 'time']
    # stance = time[round(time, 2) == 6.58].index[0]
    # tf_kin = comak_kin.loc[0:stance, tf_cols]
    # tf_kin_sum['PCLpm'] = tf_kin.filter(regex='PCLpm').sum(axis=1)
    # tf_kin_sum['PCLal'] = tf_kin.filter(regex='PCLal').sum(axis=1)
    # tf_kin_sum['MCLs'] = tf_kin.filter(regex='MCLs').sum(axis=1)
    # tf_kin_sum['MCLd'] = tf_kin.filter(regex='MCLd').sum(axis=1)
    # tf_kin_sum['MCLp'] = tf_kin.filter(regex='MCLp').sum(axis=1)
    # tf_kin_sum['ACLpl'] = tf_kin.filter(regex='ACLpl').sum(axis=1)
    # tf_kin_sum['ACLam'] = tf_kin.filter(regex='ACLam').sum(axis=1)
    # tf_kin_sum['LCL'] = tf_kin.filter(regex='LCL').sum(axis=1)
    # tf_kin_sum['PCL'] = tf_kin.filter(regex='PCL').sum(axis=1)
    # tf_kin_sum['ACL'] = tf_kin.filter(regex='ACL').sum(axis=1)
    # tf_kin_sum['time'] = time
    # tf_kin_sum.plot(subplots=True, ax=ax, legend=0, linewidth='4', x='time')

    # results generic model
    tf_kin_sum = pd.DataFrame()
    comak_kin_results = os.path.join(main_dir[0], 'results', 'joint-mechanics', 'walking_ForceReporter_forces.sto')
    comak_kin = pd.read_csv(comak_kin_results, sep='\t', header=10)
    time = comak_kin.loc[:, 'time']
    stance = time[round(time, 2) == 6.58].index[0]
    tf_kin = comak_kin.loc[0:stance, tf_cols]
    tf_kin_sum['PCLpm'] = tf_kin.filter(regex='PCLpm').sum(axis=1)
    tf_kin_sum['PCLal'] = tf_kin.filter(regex='PCLal').sum(axis=1)
    tf_kin_sum['MCLs'] = tf_kin.filter(regex='MCLs').sum(axis=1)
    tf_kin_sum['MCLd'] = tf_kin.filter(regex='MCLd').sum(axis=1)
    tf_kin_sum['MCLp'] = tf_kin.filter(regex='MCLp').sum(axis=1)
    tf_kin_sum['ACLpl'] = tf_kin.filter(regex='ACLpl').sum(axis=1)
    tf_kin_sum['ACLam'] = tf_kin.filter(regex='ACLam').sum(axis=1)
    tf_kin_sum['LCL'] = tf_kin.filter(regex='LCL').sum(axis=1)
    tf_kin_sum['PCL'] = tf_kin.filter(regex='PCL').sum(axis=1)
    tf_kin_sum['ACL'] = tf_kin.filter(regex='ACL').sum(axis=1)
    tf_kin_sum['Stance phase {%]'] = np.arange(0,100, 100/len(tf_kin))
    titles = ['PCLpm', 'PCLal', 'MCLs', 'MCLd',
               'MCLp', 'ACLpl', 'ACLam', 'LCL', 'PCL', 'ACL']
    tf_kin_sum.plot(subplots=True, ax=ax, legend=0, title=titles, linewidth='4', linestyle='dashed', x='Stance phase {%]')

    for sample in range(0,100):
        for subj_ind, subject in enumerate(subjects):
            comak_kin_results = os.path.join(main_dir[0], 'results2lig_' + str(sample), 'comak', '3DGaitModel2392_ForceReporter_forces.sto')
            try:
                comak_kin = pd.read_csv(comak_kin_results, sep='\t', header=10)
                time = comak_kin.loc[:, 'time']
                stance = time[time == 6.58].index[0]
                tf_kin = comak_kin.loc[0:stance, tf_cols]
                # tf_kin[tf_cols[3:]] = tf_kin[tf_cols[3:]]*1000  # convert to mm
                err.append(abs(tf_kin[tf_cols[:]].diff()/tf_kin[tf_cols[:]].max()).max().max())
                if abs(tf_kin[tf_cols[:]].diff()/tf_kin[tf_cols[:]].max()).max().max()>1000:
                    fail_sim_err.append([sample, abs(tf_kin[tf_cols[:]].diff()/tf_kin[tf_cols[:]].max()).max().max()])
                    # tf_kin.plot(subplots=True, legend=0, title=tf_cols, linewidth='0.5', alpha=0.5)
                else:
                    non_fail.append([sample, abs(tf_kin[tf_cols[:]].pct_change()).max().max()])
                    tf_kin_sum = pd.DataFrame()
                    tf_kin_sum['PCLpm'] = tf_kin.filter(regex='PCLpm').sum(axis=1)
                    tf_kin_sum['PCLal'] = tf_kin.filter(regex='PCLal').sum(axis=1)
                    tf_kin_sum['MCLs'] = tf_kin.filter(regex='MCLs').sum(axis=1)
                    tf_kin_sum['MCLd'] = tf_kin.filter(regex='MCLd').sum(axis=1)
                    tf_kin_sum['MCLp'] = tf_kin.filter(regex='MCLp').sum(axis=1)
                    tf_kin_sum['ACLpl'] = tf_kin.filter(regex='ACLpl').sum(axis=1)
                    tf_kin_sum['ACLam'] = tf_kin.filter(regex='ACLam').sum(axis=1)
                    tf_kin_sum['LCL'] = tf_kin.filter(regex='LCL').sum(axis=1)
                    tf_kin_sum['PCL'] = tf_kin.filter(regex='PCL').sum(axis=1)
                    tf_kin_sum['ACL'] = tf_kin.filter(regex='ACL').sum(axis=1)
                    tf_kin_sum['Stance phase {%]'] = np.arange(0,100, 100/len(tf_kin))
                    tf_kin_sum.plot(subplots=True, ax=ax, legend=0, title=titles, linewidth='1', alpha=0.2, x='Stance phase {%]')
                    if sample == 0:
                        ax[0, 0].legend(["Generic", "Personalized"])
                # bla.append(tf_kin)
            except Exception as e:
                failed.append([sample, e])
                pass

""" Kinematics and Ligament Forces"""
if kin_f == 1:
    # bla=[]
    failed = []
    fail_sim_err = []
    err = []
    non_fail = []
    fig, ax = plt.subplots(2, 3)
    main_dir = [r'C:\opensim-jam\opensim-jam-release\examples\walkingS0L']

    # results subject-specific ligament position
    # comak_kin_results = os.path.join(main_dir[0], 'results2lig', 'comak', 'walking_values.sto')
    # comak_kin = pd.read_csv(comak_kin_results, sep='\t', header=8)
    # time = comak_kin.loc[:, 'time']
    # stance = time[time == 6.58].index[0]
    # tf_kin = comak_kin.loc[0:stance, tf_cols]
    # tf_kin[tf_cols[3:]] = tf_kin[tf_cols[3:]] * 1000  # convert to mm
    # tf_kin.plot(subplots=True, ax=ax, legend=0, title=tf_cols, linewidth='4')

    # results generic model
    comak_kin_results = os.path.join(main_dir[0], 'results', 'comak', 'walking_values.sto')
    comak_kin = pd.read_csv(comak_kin_results, sep='\t', header=8)
    time = comak_kin.loc[:, 'time']
    stance = time[time == 6.58].index[0]
    tf_cols = ['knee_add_l', 'knee_rot_l', 'knee_tx_l']
    tf_kin = comak_kin.loc[0:stance, tf_cols]
    tf_kin[tf_cols[2]] = tf_kin[tf_cols[2]] * 1000  # convert to mm
    tf_kin['Stance phase [%]'] = np.arange(0, 100, 100 / len(tf_kin))
    tf_kin.plot(subplots=True, ax=ax[0], legend=0, linewidth='4', linestyle='dashed', x='Stance phase [%]')

    comak_kin_results = os.path.join(main_dir[0], 'results', 'joint-mechanics', 'walking_ForceReporter_forces.sto')
    comak_kin = pd.read_csv(comak_kin_results, sep='\t', header=10)
    time = comak_kin.loc[:, 'time']
    stance = time[round(time, 2) == 6.58].index[0]
    tf_cols = [s + '.force_total' for s in lig_femur]
    tf_kin_temp = comak_kin.loc[0:stance, tf_cols]
    tf_kin = pd.DataFrame()
    tf_kin['ACLpl'] = tf_kin_temp.filter(regex='ACLpl').sum(axis=1)
    tf_kin['ACLam'] = tf_kin_temp.filter(regex='ACLam').sum(axis=1)
    tf_kin['LCL'] = tf_kin_temp.filter(regex='LCL').sum(axis=1)
    tf_kin['Stance phase [%]'] = np.arange(0, 100, 100 / len(tf_kin))

    # titles = ['knee_add_l','knee_rot_l','knee_tx_l', 'ACLpl', 'ACLam', 'LCL']
    colours = ['#d62728', '#9467bd', '#8c564b']
    tf_kin.plot(subplots=True, ax=ax[1], legend=0, linewidth='4', linestyle='dashed', x='Stance phase [%]', color=colours)
    # ax[0, 0].legend(["Personalized", "Generic"])

    for sample in range(0,100):
        for subj_ind, subject in enumerate(subjects):

            comak_kin_results = os.path.join(main_dir[0], 'results2lig_' + str(sample), 'comak', 'walking_values.sto')
            try:
                tf_cols = ['knee_add_l', 'knee_rot_l', 'knee_tx_l']
                comak_kin = pd.read_csv(comak_kin_results, sep='\t', header=8)
                time = comak_kin.loc[:, 'time']
                stance = time[time == 6.58].index[0]
                tf_kin = comak_kin.loc[0:stance, tf_cols]
                tf_kin[tf_cols[2]] = tf_kin[tf_cols[2]]*1000  # convert to mm

                titles = ['Adduction', 'Internal Rotation', 'Anterior Translation']
                # non_fail.append([sample, abs(tf_kin[tf_cols[:]].pct_change()).max().max()])
                tf_kin['Stance phase [%]'] = np.arange(0, 100, 100 / len(tf_kin))
                tf_kin.plot(subplots=True, ax=ax[0], legend=0, title=titles, linewidth='1', alpha=0.2,
                            x='Stance phase [%]')

                comak_kin_results = os.path.join(main_dir[0], 'results2lig_' + str(sample), 'comak',
                                                 '3DGaitModel2392_ForceReporter_forces.sto')
                comak_kin = pd.read_csv(comak_kin_results, sep='\t', header=10)
                time = comak_kin.loc[:, 'time']
                stance = time[round(time, 2) == 6.58].index[0]
                tf_cols = [s + '.force_total' for s in lig_femur]
                tf_kin_temp = comak_kin.loc[0:stance, tf_cols]
                tf_kin = pd.DataFrame()
                tf_kin['ACLpl'] = tf_kin_temp.filter(regex='ACLpl').sum(axis=1)
                tf_kin['ACLam'] = tf_kin_temp.filter(regex='ACLam').sum(axis=1)
                tf_kin['LCL'] = tf_kin_temp.filter(regex='LCL').sum(axis=1)
                tf_kin['Stance phase [%]'] = np.arange(0, 100, 100 / len(tf_kin))

                titles = ['ACLpl', 'ACLam', 'LCL']
                colours = ['#d62728', '#9467bd', '#8c564b']
                tf_kin.plot(subplots=True, ax=ax[1], legend=0, title=titles, linewidth='1', alpha=0.2, x='Stance phase [%]', color=colours)
                if sample == 0:
                    ax[0, 0].legend(["Generic", "Personalized"], bbox_to_anchor=(2.0, 1.42))
                # bla.append(tf_kin)
            except Exception as e:
                failed.append([sample, e])
                pass
    ax[0, 0].set_xlabel('')
    ax[0, 1].set_xlabel('')
    ax[0, 2].set_xlabel('')
    ax[0, 0].set_ylabel('Angle [deg]')
    ax[0, 1].set_ylabel('Angle [deg]')
    ax[0, 2].set_ylabel('Translation [mm]')
    ax[1, 0].set_ylabel('Force [N]')
    ax[1, 1].set_ylabel('Force [N]')
    ax[1, 2].set_ylabel('Force [N]')
    ax[0, 0].set_xlabel('')

    fig.subplots_adjust(hspace=0.3, wspace=0.6)

line = []
dof=1
for ind in range(1,96):
    line.append(ax[1,dof].lines[ind].get_ydata())
line = np.asarray(line)
r = np.ptp(line,axis=0)
print(max(abs(r)))