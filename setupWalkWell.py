import os
import opensim as osim
import pandas as pd
import glob
from pathlib import Path

trial_info = pd.read_excel(r"C:\Users\User\Documents\WalkWell\data\WalkWell\Trial_info2.xlsx")
mocap_path = r'C:\Users\User\Documents\WalkWell\data\WalkWell'

run_ik = 1
run_id = 1
run_so = 1
run_jrl = 1

for i, pt in enumerate(trial_info["PP"]):
    participant = pt
    BW = trial_info['BW'][i]
    foot = trial_info['foot'][i]
    test_leg = True
    full_trial = trial_info['mocap_trial'][i]
    path = os.path.join(mocap_path,pt)

    os_path = os.path.join(path,'opensim')

    # model = osim.Model(os.path.join(os_path,'P01.osim'))
    # model.initSystem()

    external_loads = osim.ExternalLoads(os.path.join(mocap_path, 'GRF.xml'), True)
    external_loads.setDataFileName(os.path.join(os_path, f'{full_trial}_forces.mot'))
    external_loads.printToXML(os.path.join(os_path, f'{full_trial}_GRF.xml'))

    search_pattern = os.path.join(os_path, f"*{full_trial}*.trc")
    trc_files = glob.glob(search_pattern)
    trc_files = trc_files[1:]
    for i, trc_step in enumerate(trc_files):
        trial = Path(trc_step).stem

        trc_data = pd.read_csv(trc_step, skiprows=4, sep="\t")
        time = trc_data['Unnamed: 1'].values
        end_time = time[-1]  # (trc_data.getNumRows() - 10) / 100
        start_time = time[0]

        if run_ik == 1:
            if not os.path.isfile(os.path.join(os_path, f'{trial}_IK.mot')):
                inverse_kinematics_tool = osim.InverseKinematicsTool(os.path.join(mocap_path,'setupIK.xml'))
                inverse_kinematics_tool.setName(pt + trial)
                inverse_kinematics_tool.set_time_range(0,start_time)
                inverse_kinematics_tool.set_time_range(1, end_time)
                inverse_kinematics_tool.set_model_file(os.path.join(os_path,f'{pt}.osim'))
                inverse_kinematics_tool.set_output_motion_file(os.path.join(os_path,f'{trial}_IK.mot'))
                inverse_kinematics_tool.set_marker_file(os.path.join(os_path,f'{trial}.trc'))
                inverse_kinematics_tool.printToXML(os.path.join(os_path,f'{trial}_IK_setup.xml'))

        # external_loads = osim.ExternalLoads(os.path.join(mocap_path, 'GRF.xml'),True)
        # external_loads.setDataFileName(os.path.join(os_path, f'{trial}.mot'))
        # external_loads.printToXML(os.path.join(os_path, f'{trial}_GRF.xml'))

        if run_id == 1:
            if not os.path.isfile(os.path.join(os_path, f'{trial}_ID.mot')):
                    inverse_dynamics_tool = osim.InverseDynamicsTool(os.path.join(mocap_path,'setupID.xml'))
                    inverse_dynamics_tool.setName(pt + trial)
                    inverse_dynamics_tool.setModelFileName(os.path.join(os_path,f'{pt}.osim'))
                    inverse_dynamics_tool.set_results_directory(os_path)
                    inverse_dynamics_tool.setStartTime(start_time)
                    inverse_dynamics_tool.setEndTime(end_time)
                    inverse_dynamics_tool.setExternalLoadsFileName(os.path.join(os_path,f'{trial}_GRF.xml'))
                    inverse_dynamics_tool.setCoordinatesFileName(os.path.join(os_path,f'{trial}_IK.mot'))
                    inverse_dynamics_tool.setOutputGenForceFileName(f'{trial}_ID.mot')
                    inverse_dynamics_tool.printToXML(os.path.join(os_path,f'{trial}_ID_setup.xml'))

        if run_so == 1:
            if not os.path.isfile(os.path.join(os_path, pt + trial + '_StaticOptimization_force.sto')):
                so_tool = osim.AnalyzeTool(os.path.join(mocap_path, 'setupSO.xml'))
                so_tool.setName(pt + trial)
                so_tool.setModelFilename(os.path.join(os_path, f'{pt}.osim'))
                so_tool.setForceSetFiles(
                    osim.ArrayStr(os.path.join(os_path, 'SO_Actuators.xml'), 1))
                so_tool.getForceSetFiles()
                so_tool.setResultsDir(os_path)
                so_tool.setInitialTime(start_time)
                so_tool.setFinalTime(end_time)
                # so = analyze_tool.getAnalysisSet().get(0)
                # so.setStartTime(start_time)
                so_tool.setExternalLoadsFileName(os.path.join(os_path, f'{full_trial}_GRF.xml'))
                so_tool.setCoordinatesFileName(os.path.join(os_path, f'{trial}_IK.mot'))
                so_tool.printToXML(os.path.join(os_path, f'{trial}_SO_setup.xml'))

        if run_jrl == 1:
            if not os.path.isfile(os.path.join(os_path, pt + trial + '_JointReaction_ReactionLoads.sto')):
                analyze_tool = osim.AnalyzeTool(os.path.join(mocap_path,'setupAnalyze.xml'))
                analyze_tool.setName(pt + trial)
                analyze_tool.setModelFilename(os.path.join(os_path,f'{pt}.osim'))
                analyze_tool.setForceSetFiles(osim.ArrayStr(r'C:\Users\User\Documents\WalkWell\data\WalkWell\SO_Actuators.xml',1))
                analyze_tool.setResultsDir(os_path)
                analyze_tool.setInitialTime(start_time)
                analyze_tool.setFinalTime(end_time)
                jrl = osim.JointReaction.safeDownCast(analyze_tool.getAnalysisSet().get(0))
                jrl.setForcesFileName(os.path.join(os_path, pt + trial + '_StaticOptimization_force.sto'))
                analyze_tool.setExternalLoadsFileName(os.path.join(os_path, f'{full_trial}_GRF.xml'))
                analyze_tool.setCoordinatesFileName(os.path.join(os_path,f'{trial}_IK.mot'))
                analyze_tool.printToXML(os.path.join(os_path, f'{trial}_Analyze_setup.xml'))

        if run_ik == 1:
            if not os.path.isfile(os.path.join(os_path,f'{trial}_IK.mot')):
                try:
                    inverse_kinematics_tool.run()
                except:
                    print(f'{trial}_IK.mot' + ' failed')
                    pass
        if run_id == 1:
            if not os.path.isfile(os.path.join(os_path, f'{trial}_ID.mot')):
                try:
                    inverse_dynamics_tool.run()
                except:
                    print(f'{trial}_ID.mot' + ' failed')
                    pass
        if run_so == 1:
            if not os.path.isfile(os.path.join(os_path, pt + trial + '_StaticOptimization_force.sto')):
                try:
                    so_tool2 = osim.AnalyzeTool(os.path.join(os_path, f'{trial}_SO_setup.xml'))
                    so_tool2.run()
                except:
                    print(f'{trial} SO' + ' failed')
                    pass
        if run_jrl == 1:
            if not os.path.isfile(os.path.join(os_path, pt + trial + '_JointReaction_ReactionLoads.sto')):
                try:
                    analyze_tool2 = osim.AnalyzeTool(os.path.join(os_path, f'{trial}_Analyze_setup.xml'))
                    analyze_tool2.run()
                except:
                    print(f'{trial} JRA' + ' failed')
                    pass


