import opensim as osim

sto = osim.STOFileAdapterForReading()
force_file = r'Y:\MultiscaleModelling\Data\S0\GaitAnalysis\Mariska\OpenSim\12DoF\joint-mechanics' \
             r'\walking_ForceReporter_forces.sto'
data = sto.read(force_file)