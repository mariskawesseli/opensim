from ImportMOBILabData import *
import pandas as pd
import numpy as np


def get_mocap_path(path, file_part):
    for root, dirs, files in os.walk(path):
        for file in files:
            # print(f"Checking file: {file}")  # Debug: Show each file being checked
            if file_part in file:  # Check if part of the name matches
                matched_path = os.path.join(root, file)
                # print(f"Match found: {matched_path}")  # Debug: Show matched path
                print("Match found for " + file_part)
                return matched_path  # Return path immediately upon finding a match
    print("No match found for " + file_part)  # Debug: Indicate if no match was found
    return None  # Return None if no matching file is found


trial_info = pd.read_excel(r"C:\Users\User\Documents\WalkWell\data\WalkWell\Trial_info2.xlsx")
mocap_path = r'C:\Users\User\Documents\WalkWell\data\WalkWell'

for i, pt in enumerate(trial_info["PP"]):
    participant = pt
    print(participant)
    BW = trial_info['BW'][i]
    foot = trial_info['foot'][i]
    test_leg = True
    trial = trial_info['mocap_trial'][i]
    path = os.path.join(mocap_path,pt)
    c3d_file = get_mocap_path(path, trial + '.c3d')

    newpath = os.path.join(path,'opensim')
    if not os.path.exists(newpath):
        os.makedirs(newpath)

    output_file = trial
    cutoff_grf = 50
    filter_grf = 0  # is it working correctly?
    height = [0,0]  # is it working correctly?
    rotate_for_pos_x = 1
    force_data = 1

    force_label = ['ground_force_1_vx', 'ground_force_1_vy', 'ground_force_1_vz', 'ground_force_1_px', 'ground_force_1_py',
                    'ground_force_1_pz', 'ground_moment_1_mx', 'ground_moment_1_my', 'ground_moment_1_mz']

    # Set environment path
    # os.environ['PATH'] = opensim_bin + os.pathsep + os.environ['PATH']

    # Get the path to a C3D file
    c3dpath = c3d_file

    # Construct an opensimC3D object with input c3d path
    # Constructor takes full path to c3d file and an integer for forceplate
    # representation (1 = COP).
    adapter = osim.C3DFileAdapter()
    adapter.setLocationForForceExpression(1)
    c3d = adapter.read(c3dpath)
    c3d.ForceLocation = 'CenterOfPressure'
    forces = adapter.getForcesTable(c3d)
    markers = adapter.getMarkersTable(c3d)

    # Get some stats...
    # Get the number of marker trajectories
    nTrajectories = markers.getNumColumns()
    # Get the marker data rate
    rMakers = markers.getTableMetaDataString('DataRate')
    # Get the number of forces
    nForces = forces.getNumColumns()/3
    # Get the force data rate
    if force_data == 1:
        rForces = forces.getTableMetaDataString('DataRate')

    # Get Start and end time
    t0 = markers.getIndependentColumn()[0]
    tn = markers.getIndependentColumn()[-1]

    # translate CoP if height on FP
    if (np.array(height) > 0).sum() > 0:
        adjustHeight(forces, height=height)

    # Rotate the data
    markers = rotateTable(markers, 'x', -90)
    forces = rotateTable(forces, 'x', -90)

    if rotate_for_pos_x == 1:
        markers = rotateTable(markers, 'y', 90)
        forces = rotateTable(forces, 'y', 90)

    # Convert point and torque data are in mm and Nmm and
    # to m and Nm
    if markers.getTableMetaDataString('Units') == 'mm' and force_data == 1:
        convertMillimeters2Meters(forces)

    if cutoff_grf >= 0:
        cutoffForceData(forces, cutoff_frequency=cutoff_grf)

    # Create correct force labels
    new_force_label = []
    for i in range(0, int(nForces)):
        temp_label = [sub.replace('1', str(i+1)) for sub in force_label]
        new_force_label = new_force_label + temp_label

    # Get the c3d in different forms
    markerTable = markers.flatten()
    if force_data == 1:
        forceTable = forces.flatten()
        forceTable.setColumnLabels(new_force_label)

    if filter_grf == 1:
        adapter = osim.C3DFileAdapter()
        adapter.setLocationForForceExpression(0)
        c3d = adapter.read(c3dpath)
        # c3d.ForceLocation = 'OriginOfForcePlate'
        forces2 = adapter.getForcesTable(c3d)
        forceTable2 = forces2.flatten()
        forceTable2.setColumnLabels(new_force_label)
        filterGRF(forceTable2, rForces)

    # columnData = forceTable.getDependentColumnAtIndex(1)
    # f1 = columnData.to_numpy()
    # columnData = forceTable.getDependentColumnAtIndex(1+9)
    # f2 = columnData.to_numpy()
    # plt.plot(f1)
    # plt.plot(f2)
    # # plt.ylim(-100,100)
    # plt.show(block=False)
    # bla
    # Write the marker and force data to file
    # Write marker data to trc file.
    trcAdapter = osim.TRCFileAdapter()
    trcAdapter.write(markers, os.path.join(newpath, output_file + '_markers.trc'))

    # Write force data to mot file.
    if force_data == 1:
        motAdapter = osim.STOFileAdapter()
        motAdapter.write(forceTable, os.path.join(newpath, output_file + '_forces.mot'))

