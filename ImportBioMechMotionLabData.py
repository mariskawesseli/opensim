import os
import opensim as osim
import numpy as np
from scipy import signal
# import matplotlib.pyplot as plt
# import matplotlib
# matplotlib.use("TkAgg")


def rotateTable(table, axisString, value):
    # Private Method for doing the table rotation operations

    # set up the transform
    if axisString == 'x':
        axis = osim.CoordinateAxis(0)
    elif axisString == 'y':
        axis = osim.CoordinateAxis(1)
    elif axisString == 'z':
        axis = osim.CoordinateAxis(2)
    else:
        ValueError('input axis must ne either x,y, or z')

    # instantiate a transform selfect. Rotation() is a Simbody class
    R = osim.Rotation(np.deg2rad(value), axis)

    # Rotation() works on each row.
    for iRow in range(0, table.getNumRows() - 1):
        # get a row from the table
        rowVec = table.getRowAtIndex(iRow)
        # rotate each Vec3 element of row vector, rowVec, at once
        rowVec_rotated = R.multiply(rowVec)
        # overwrite row with rotated row
        table.setRowAtIndex(iRow, rowVec_rotated)

    return table


def convertMillimeters2Meters(forces):
    # Function to convert  point data (mm) to m and Torque data
    # (Nmm) to Nm.

    nRows = forces.getNumRows()
    labels = forces.getColumnLabels()

    for ind in range(0, int(forces.getNumColumns()) - 1):
        # All force columns will have the 'f' prefix while point
        # and moment columns will have 'p' and 'm' prefixes,
        # respectively.
        if not str(labels[ind]).startswith('f'):
            columnData = forces.getDependentColumnAtIndex(ind)
            for u in range(0, nRows - 1):
                # Divide by 1000
                columnData.__setitem__(u,columnData.getElt(ind,u).scalarDivideEq(1000))
    print('Point and Torque values convert from mm and Nmm to m and Nm, respectively')


def cutoffForceData(forces, cutoff_frequency = 0):
    # Function to convert  point data (mm) to m and Torque data
    # (Nmm) to Nm.

    nRows = forces.getNumRows()
    labels = forces.getColumnLabels()

    for ind in range(0, int(forces.getNumColumns())):
        # All force columns will have the 'f' prefix while point
        # and moment columns will have 'p' and 'm' prefixes,
        # respectively.
        if str(labels[ind]).startswith('f'):
            columnData = forces.getDependentColumnAtIndex(ind)
            for u in range(0, nRows):
                if columnData.getElt(ind, u).get(1) < cutoff_frequency:  # to_numpy()[1]
                    forces.getDependentColumnAtIndex(ind).getElt(u,0).setToZero()
                    forces.getDependentColumnAtIndex(ind+1).getElt(u,0).setToZero()
                    forces.getDependentColumnAtIndex(ind+2).getElt(u,0).setToZero()


def filterGRF(forceTable, sF):
    # Filter
    # not working yet!
    fco = 50
    n = 4
    fs = float(sF)
    b, a = signal.butter(n, fco / (fs / 2), btype='low', analog=False, output='ba', fs=None)

    f_filt = []
    nRows = forceTable.getNumRows()
    labels = forceTable.getColumnLabels()
    for ind in range(0, int(forceTable.getNumColumns())):
        if 'v' in str(labels[ind]) or 'moment' in str(labels[ind]):
            columnData = forceTable.getDependentColumnAtIndex(ind)
            f = np.empty((1,columnData.nrow()))
            for row in range(0,columnData.nrow()):
                f[0,row] = columnData.getElt(0,row)
            # f = columnData.to_numpy()
            # Filtering	the	data
            Forces_filt = signal.filtfilt(b, a, f)
            f_filt.append(Forces_filt)

    h = 0
    CoP = []
    for ind in range(0,int(len(f_filt)/6)):
        f_use = f_filt[ind*6:ind*6+6]
        CoP.append((-h * f_use[1] + f_use[3])/f_use[2])  # (-h*Fy+Mx)/Fz - Vicon = (-h*Fx+Mz)/Fy - OpenSim
        CoP.append((-h * f_use[0] - f_use[4])/f_use[2])  # (-h*Fx-My)/Fz - Vicon = (-h*Fy-Mx)/Fy - OpenSim
        # CoP.append((-1 * f_use[1] + f_use[3]) / f_use[2])  # COPx = (-1*My + dz*Fx)./Fz
        # CoP.append((-h * f_use[0] - f_use[4]) / f_use[2])  # COPy = (Mx + dz*Fy)./Fz
        CoP.append(np.ones((1, nRows)) * h)

    tel = 0
    for ind in range(0, int(forceTable.getNumColumns())):
        if 'p' in str(labels[ind]):
            columnData = forceTable.getDependentColumnAtIndex(ind)
            for u in range(0, nRows - 1):
                if CoP[tel][0,u] > 10:
                    columnData.__setitem__(u, 0)
                else:
                    columnData.__setitem__(u, float(CoP[tel][0,u]))
            tel=tel+1

    print('Data filtered')

    # plt.plot(np.arange(nRows), CoP[0])
    # columnData = forceTable.getDependentColumnAtIndex(2)
    # px = columnData.to_numpy()
    # plt.plot(np.arange(nRows), px)
    # plt.ylim(0,3)
    # plt.show()

    # columnData = forceTable2.getDependentColumnAtIndex(8)
    # px = columnData.to_numpy()
    # nRows = forceTable2.getNumRows()
    # plt.plot(np.arange(nRows), px)

    # bla = f_filt[5] / f_filt[1]
    # plt.plot(np.arange(nRows),f_filt[3])
    # plt.show()

    # Fx	    Fy	    Fz	        Mx	                My	            Mz
    # neg col0	col1	neg col2	neg	col	6 / 1000	col	7 / 1000	neg	col	8 / 1000
    return CoP


def adjustHeight(forces, height=0):
    # adjust the grf to a height placed on the forceplate (e.g. stairs)

    if isinstance(height, int):
        height = [height]
    nRows = forces.getNumRows()
    labels = forces.getColumnLabels()
    for ind in range(0, int(forces.getNumColumns())):
        # All force columns will have the 'f' prefix while point
        # and moment columns will have 'p' and 'm' prefixes,
        # respectively.
        if str(labels[ind]).startswith('p'):
            fp_no = int(str(labels[ind]).split('p')[1])-1
            h = height[fp_no]
            columnData = forces.getDependentColumnAtIndex(ind)
            for u in range(0, nRows):
                force_elt = forces.getDependentColumnAtIndex(ind-1).getElt(u,0).to_numpy()
                cop_elt = columnData.getElt(ind, u).to_numpy()
                if not force_elt[2] == 0:
                    k = h/force_elt[2]
                    x = cop_elt[0] + k*force_elt[0]
                    y = cop_elt[1] + k*force_elt[1]
                    z = h
                    columnData.__setitem__(u, osim.Vec3.createFromMat(np.array((x, y, z))))


if __name__ == '__main__':
    """
    Code to import c3d data from the TU Delft BioMEchMotionLab to Opensim
    Mariska Wesseling 01/2022
    Update to opensim 4.5 03/2024
    """
    # Variables
    # opensim_bin = r"C:\OpenSim 4.1\bin"
    path = r"C:\Users\User\Documents\MOBI\data\21032024"
    c3d_file = r"A_21_3_24 Squat 04.c3d"
    output_file = 'A_21_3_24 Squat 04'
    cutoff_grf = 10
    filter_grf = 0  # is it working correctly?
    height = [0,0]  # is it working correctly?
    rotate_for_pos_x = 0
    force_data = 1

    force_label = ['ground_force_1_vx', 'ground_force_1_vy', 'ground_force_1_vz', 'ground_force_1_px', 'ground_force_1_py',
                    'ground_force_1_pz', 'ground_moment_1_mx', 'ground_moment_1_my', 'ground_moment_1_mz']

    # Set environment path
    # os.environ['PATH'] = opensim_bin + os.pathsep + os.environ['PATH']

    # Get the path to a C3D file
    c3dpath = os.path.join(path,c3d_file)

    # Construct an opensimC3D object with input c3d path
    # Constructor takes full path to c3d file and an integer for forceplate
    # representation (1 = COP).
    adapter = osim.C3DFileAdapter()
    adapter.setLocationForForceExpression(1)
    c3d = adapter.read(c3dpath)
    c3d.ForceLocation = 'CenterOfPressure'
    forces = adapter.getForcesTable(c3d)
    markers = adapter.getMarkersTable(c3d)

    # Get some stats...
    # Get the number of marker trajectories
    nTrajectories = markers.getNumColumns()
    # Get the marker data rate
    rMakers = markers.getTableMetaDataString('DataRate')
    # Get the number of forces
    nForces = forces.getNumColumns()/3
    # Get the force data rate
    if force_data == 1:
        rForces = forces.getTableMetaDataString('DataRate')

    # Get Start and end time
    t0 = markers.getIndependentColumn()[0]
    tn = markers.getIndependentColumn()[-1]

    # translate CoP if height on FP
    if (np.array(height) > 0).sum() > 0:
        adjustHeight(forces, height=height)

    # Rotate the data
    markers = rotateTable(markers, 'x', -90)
    forces = rotateTable(forces, 'x', -90)

    if rotate_for_pos_x == 1:
        markers = rotateTable(markers, 'y', 180)
        forces = rotateTable(forces, 'y', 180)

    # Convert point and torque data are in mm and Nmm and
    # to m and Nm
    if markers.getTableMetaDataString('Units') == 'mm':
        convertMillimeters2Meters(forces)

    if cutoff_grf >= 0:
        cutoffForceData(forces, cutoff_frequency=cutoff_grf)

    # Create correct force labels
    new_force_label = []
    for i in range(0, int(nForces)):
        temp_label = [sub.replace('1', str(i+1)) for sub in force_label]
        new_force_label = new_force_label + temp_label

    # Get the c3d in different forms
    markerTable = markers.flatten()
    if force_data == 1:
        forceTable = forces.flatten()
        forceTable.setColumnLabels(new_force_label)

    if filter_grf == 1:
        adapter = osim.C3DFileAdapter()
        adapter.setLocationForForceExpression(0)
        c3d = adapter.read(c3dpath)
        # c3d.ForceLocation = 'OriginOfForcePlate'
        forces2 = adapter.getForcesTable(c3d)
        forceTable2 = forces2.flatten()
        forceTable2.setColumnLabels(new_force_label)
        filterGRF(forceTable2, rForces)

    # columnData = forceTable.getDependentColumnAtIndex(1)
    # f1 = columnData.to_numpy()
    # columnData = forceTable.getDependentColumnAtIndex(1+9)
    # f2 = columnData.to_numpy()
    # columnData = forceTable.getDependentColumnAtIndex(1+18)
    # f3 = columnData.to_numpy()
    # plt.plot(f1)
    # plt.plot(f2)
    # plt.plot(f3)
    # # plt.ylim(-100,100)
    # plt.show()

    # Write the marker and force data to file
    # Write marker data to trc file.
    trcAdapter = osim.TRCFileAdapter()
    trcAdapter.write(markers, os.path.join(path, output_file + '_markers.trc'))

    # Write force data to mot file.
    if force_data == 1:
        motAdapter = osim.STOFileAdapter()
        motAdapter.write(forceTable, os.path.join(path, output_file + '_forces.mot'))

