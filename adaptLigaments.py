import trimesh
import numpy as np
from scipy import interpolate
import matplotlib.pyplot as plt
import os
import xml.etree.ElementTree as ET
import glob
import sys

# sys.path.remove(r'C:\\OpenSim 4.3\\plugins')
# paths_to_remove = [r'C:\OpenSim 4.3\bin', r'C:\OpenSim 4.3\plugins', r'C:\OpenSim 4.3\sdk\Python']
# paths = os.environ["PATH"].split(os.pathsep) # split the PATH variable into a list of paths
# paths = [path for path in paths if path not in paths_to_remove] # remove the paths you want to remove
# new_path = os.pathsep.join(paths) # join the paths back into a single string separated by the OS path separator
# # os.environ["PATH"] = new_path # set the updated PATH variable
os.environ['PATH'] = r"C:\OpenSim 4.1\bin" + os.pathsep + os.environ['PATH']
import opensim
opensim.LoadOpenSimLibrary('C:\OpenSim 4.1\plugins\jam_plugin')
import matplotlib
matplotlib.use('TkAgg')


def interpolate_lig_points(lig_points, no_points, plot=0):
    from scipy.stats import gaussian_kde
    x = lig_points[:, 0]
    y = lig_points[:, 1]
    z = lig_points[:, 2]
    goon = 1
    n = 0
    # Create a uniformly spaced grid
    while goon == 1:
        steps = no_points / 2 + n  # number of rows and columns for the grid
        grid_steps = complex(str(steps) + 'j')

        interp = interpolate.Rbf(x, y, z, function='thin_plate')
        yi, xi = np.mgrid[min(lig_points[:, 1]):max(lig_points[:, 1]):grid_steps,
                 min(lig_points[:, 0]):max(lig_points[:, 0]):grid_steps]
        zi = interp(xi, yi)

        inds_remove = []
        inds_nan = []
        diff_val = []
        xi_nan = xi
        yi_nan = yi
        zi_nan = zi
        for i in range(0, len(xi)):
            for j in range(0, len(xi)):
                diff = np.linalg.norm(lig_points[:, :] - np.asarray([xi[i, j], yi[i, j], zi[i, j]]), axis=1)
                diff_val.append(np.abs(np.amin(diff)))
                if np.amin(diff) > 2:
                    inds_remove.append([i * steps + j])
                    inds_nan.append([i, j])
                    xi_nan[i, j] = np.nan
                    yi_nan[i, j] = np.nan
                    zi_nan[i, j] = np.nan
        n = n + 1
        if np.count_nonzero(~np.isnan(xi_nan)) >= no_points or n == 100:
            # print(str(np.count_nonzero(~np.isnan(xi_nan))) + ' ' + str(no_points))
            goon = 0
        diff_val = np.zeros([len(xi_nan), len(xi_nan)])
        if np.count_nonzero(~np.isnan(xi_nan)) > no_points:
            to_remove = np.count_nonzero(~np.isnan(xi_nan)) - no_points
            for i in range(0, len(xi_nan)):
                for j in range(0, len(xi_nan)):
                    diff = np.linalg.norm(lig_points[:, :] - np.asarray([xi_nan[i, j], yi_nan[i, j], zi_nan[i, j]]),
                                          axis=1)
                    diff_val[i, j] = np.abs(np.amin(diff))
            for k in range(0, to_remove):
                i, j = np.unravel_index(np.nanargmax(diff_val), diff_val.shape)
                diff_val[i, j] = np.nan
                xi_nan[i, j] = np.nan
                yi_nan[i, j] = np.nan
                zi_nan[i, j] = np.nan
        # print('-1 ' + str(np.count_nonzero(~np.isnan(xi_nan))) + ' ' + str(no_points))

    if len(lig_points) == 2:
        tck, u = interpolate.splprep([lig_points[:, 0], lig_points[:, 1], lig_points[:, 2]], s=10, k=1)
        x_knots, y_knots, z_knots = interpolate.splev(tck[0], tck)
        u_fine = np.linspace(0, 1, no_points)
        x_fine, y_fine, z_fine = interpolate.splev(u_fine, tck, der=0)
        # lig_points_osim = np.transpose(np.asarray([x_fine, y_fine, z_fine]))
        xi_nan, yi_nan, zi_nan = x_fine, y_fine, z_fine

    lig_points_osim = xi_nan[np.logical_not(np.isnan(xi_nan))] / 1000, yi_nan[np.logical_not(np.isnan(yi_nan))] / 1000, \
                      zi_nan[np.logical_not(np.isnan(zi_nan))] / 1000
    lig_points_osim = np.asarray(lig_points_osim)
    i = np.argsort(lig_points_osim[0, :])
    lig_points_osim = lig_points_osim[:, i]
    if plot == 1:
        fig2 = plt.figure()
        plt.ioff()
        ax3d = fig2.add_subplot(111, projection='3d')
        ax3d.plot(lig_points[:, 0], lig_points[:, 1], lig_points[:, 2], 'r*')
        # ax3d.plot(x_knots, y_knots, z_knots, 'bo')
        # ax3d.plot(x_fine, y_fine, z_fine, 'go')
        ax3d.scatter(xi_nan[:], yi_nan[:], zi_nan[:], c='g')
        # fig2.show()
        plt.show()

    return lig_points_osim


def adapt_setup(file, tags, input, tool):
    if tool == 'COMAKInverseKinematicsTool':
        tags_ik = tags[3:]
        input_ik = input[3:]
        tags = tags[0:3]
        input = input[0:3]

    tree = ET.parse(file)
    root = tree.getroot()
    if tool == 'COMAKInverseKinematicsTool':
        for i, tag in enumerate(tags_ik):
            elem = root.find(tool).find('InverseKinematicsTool').find(tag)
            elem.text = input_ik[i]
    for i, tag in enumerate(tags):
        elem = root.find(tool).find(tag)
        elem.text = input[i]

    tree.write(file)


# TODO currently need to remove the contact from osim file as this is not recognized
#  - error: Process finished with exit code -1073741819 (0xC0000005)
subjects = ['1']  #['S0']  # [9,13,19,23,26,29,32,35,37,41]
sides = ['R']
segments = ['femur','tibia', 'fibula']
short_ssm = [0, 1, 0]
no_particles = [4096, 4096, 2048]
opensim_meshes = ['smith2019-R-femur-bone_remesh.stl','smith2019-R-tibia-bone_remesh.stl',
                  'smith2019-R-fibula-bone_remesh.stl']
run_fit = 1
run_find_points = 1
run_update_mesh = 1
create_cmd = 0
add_contact = 1
for_linux = 0

opensim_geometry_folder = r'C:\opensim-jam\jam-resources\jam-resources-main\models\knee_healthy\smith2019\Geometry'
input_file_folder = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output'
gen_model = r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\4DCT\1\lenhart2015_nocontact.osim'  # generic scaled model
osim_path = r"C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\4DCT\1"

data_folder = r"C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\4DCT/"
path = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/'
cadaver_folder = r'C:\Users\mariskawesseli\Documents\LigamentStudy\ImageData/'

lig_femur = ["PCLpm1", "PCLpm2", "PCLpm3", "PCLpm4", "PCLpm5", "PCLal1", "PCLal2", "PCLal3", "PCLal4", "PCLal5",
             "MCLs1", "MCLs2", "MCLs3", "MCLs4", "MCLs5", "MCLs6", "MCLd1", "MCLd2", "MCLd3", "MCLd4", "MCLd5", "MCLp1",
             "MCLp2", "MCLp3","MCLp4", "MCLp5", "ACLpl1", "ACLpl2", "ACLpl3", "ACLpl4", "ACLpl5", "ACLpl6", "ACLam1",
             "ACLam2", "ACLam3", "ACLam4", "ACLam5", "ACLam6", "LCL1", "LCL2", "LCL3", "LCL4"]
lig_tibia = ["PCLpm1", "PCLpm2", "PCLpm3", "PCLpm4", "PCLpm5", "PCLal1", "PCLal2", "PCLal3", "PCLal4", "PCLal5",
             "MCLd1", "MCLd2", "MCLd3", "MCLd4", "MCLd5", "MCLp1", "MCLp2", "MCLp3", "MCLp4", "MCLp5", "ACLpl1",
             "ACLpl2", "ACLpl3", "ACLpl4", "ACLpl5", "ACLpl6", "ACLam1", "ACLam2", "ACLam3", "ACLam4", "ACLam5",
             "ACLam6"]
lig_fibula = ["LCL1", "LCL2", "LCL3", "LCL4"]

for sample in range(0,1):  # 100

    for subj_ind, subject in enumerate(subjects):
        osim_model = gen_model
        model = opensim.Model(osim_model)
        state = model.initSystem()
        forces = model.getForceSet()

        if sides[subj_ind] == 'R':
            side = '_R'
            reflect = ''
        else:
            side = '_L'
            reflect = '.reflect'

        for seg_ind, segment in enumerate(segments):
            if short_ssm[seg_ind]:
                short = '_short'
            else:
                short = ''

            if segment == 'femur':
                lig = lig_femur
            elif segment == 'fibula':
                lig = lig_fibula
            else:
                lig = lig_tibia

            ssm_path = path + segment + '_bone' + short + r'\new_bone_mri\shape_models/'
            new_path = ssm_path + '/fit/'
            ssm_files = glob.glob(ssm_path + "*.stl")
            mesh_inp = new_path + r'\SSM_' + segment + short + '.stl'  # ssm_files[subj_ind]  # os.path.join(input_file_folder, segment + r'_bone' + short, 'new_bone_mri', 'input', subject + side + '_' + segment + '.stl')
            out_file = os.path.join(data_folder, str(subject), 'input_mesh_' + segment + short + '_translated.stl')
            out_file2 = os.path.join(data_folder, str(subject),'input_mesh_' + segment + short + '_icp.stl')
            out_model = osim_path + r'\lig_' + str(sample) + '.osim'

            if segment == 'femur':
                body = 'femur_distal_r'  # 'femur_distal_l'
            else:
                body = 'tibia_proximal_r'  # 'tibia_proximal_l'

            if run_fit == 1:
                scale_factor = model.getBodySet().get(body).get_attached_geometry(0).get_scale_factors().to_numpy()

                # run ICP to get final position SSM point cloud on original mesh
                # mesh OpenSim model -  make sure this is high quality (but file not too big) and file is in millimeters
                opensim_mesh = os.path.join(opensim_geometry_folder,opensim_meshes[seg_ind])
                mesh1 = trimesh.load_mesh(opensim_mesh)
                # mesh segmented from MRI
                mesh2 = trimesh.load_mesh(mesh_inp)

                xaxis, yaxis, zaxis = [1, 0, 0], [0, 1, 0], [0, 0, 1]

                # scale generic mesh for subject
                origin = mesh1.center_mass
                S = trimesh.transformations.scale_matrix(scale_factor[0], origin, xaxis)
                S1 = trimesh.transformations.scale_matrix(scale_factor[1], origin, yaxis)
                S2 = trimesh.transformations.scale_matrix(scale_factor[2], origin, zaxis)
                mesh1.apply_transform(trimesh.transformations.concatenate_matrices(S, S1, S2))

                # Rotate segmented bone (depend on orientation of the MRI)
                Rx = trimesh.transformations.rotation_matrix(-90 / (180 / np.pi), xaxis)
                Ry = trimesh.transformations.rotation_matrix(90 / (180 / np.pi), yaxis)
                R = trimesh.transformations.concatenate_matrices(Ry, Rx)
                mesh2.apply_transform(R)

                # Translate segmented mesh to OpenSim bone location
                T = trimesh.transformations.translation_matrix(mesh1.center_mass - mesh2.center_mass)
                mesh2.apply_transform(T)
                mesh2.export(out_file)  # export translated file

                # ICP to fit segmented bone to OpenSim mesh
                kwargs = {"scale": False}
                icp, mesh2T, cost = trimesh.registration.icp(mesh2.vertices, mesh1, initial=np.identity(4), threshold=1e-2, max_iterations=50, **kwargs)
                mesh2.apply_transform(icp)
                mesh2.export(out_file2)
                # scale, shear, angles, trans, persp = trimesh.transformations.decompose_matrix(icp)  # to check transformation

                ligs = trimesh.load_mesh(new_path + r'\SSM_' + segment + '_pred_points_color.xyz')
                ligs.apply_transform(R)
                ligs.apply_transform(T)
                ligs.apply_transform(icp)
                np.savetxt(new_path + r'\SSM_' + segment + short + '_pred_points_all_osim.xyz', ligs.vertices, delimiter=" ")

            if run_find_points == 1:
                # Load which SSM points are related to which ligaments
                occurances = np.load(os.path.join(cadaver_folder, 'occurances_' + segment + short + '.npz'))
                occ = np.load(os.path.join(cadaver_folder, 'all_occurances_' + segment + '.npz'))
                # occurances = [occ['PCL'], occ['MCLp'], occ['MCLd'], occ['post_obl'], occ['ACL'], occ['LCL'], occ['pop']]
                # occ = occurances

                ligs = trimesh.load_mesh(new_path + r'\SSM_' + segment + short + '_pred_points_all_osim.xyz') #_all
                cols = np.loadtxt(new_path + r'\SSM_' + segment + '_pred_points_color.xyz')[:, 3]

                if segment != 'fibula':
                    no_PCL_points = occ['PCL'].size
                    osim_points = 5
                    PCL = ligs.vertices[0:no_PCL_points]
                    if segment == 'femur':
                        # al = [0, 1, 2, 3, 4, 5, 7, 10, 11, 13, 29, 30, 31, 34, 35, 39, 40, 42, 43, 47, 49, 50, 51, 55, 56, 57, 58,
                        #         59, 24, 27, 28, 48]  # when using half of specimens to be included
                        al = [98, 105, 77, 90, 79, 75, 71, 49, 48, 43, 56, 87, 86, 65, 58, 29, 28, 88, 13, 70, 110, 2, 7, 42, 59,
                              57, 66, 106, 89, 72, 51, 47, 27, 4, 0, 73, 109, 108, 55, 3, 11, 39, 100, 61, 40, 34, 24, 1, 5, 91, 107, 76, 10, 35, 83, 50]  # when including all points
                    elif segment == 'tibia':
                        # al = [0, 1, 2, 4, 6, 7, 10, 12, 13, 19, 21, 22, 23, 24, 25, 26, 27, 31, 32, 34, 35, 40, 45, 46, 47, 50, 52,
                        #         56]  # when using half of specimens to be included, 59, 60, 61, 62, 63, 65, 70
                        al = [167, 171, 174, 168, 165, 166, 123, 127, 126, 125, 172, 81, 80, 79, 72, 85, 88, 87, 86, 169, 144,
                              156, 139, 136, 185, 122, 124, 89, 90, 58, 51, 48, 67, 38, 84, 180, 177, 105, 68, 37, 45, 49, 30,
                              43, 44, 103, 120, 178, 115, 100, 36, 14, 16, 15, 17, 3, 42, 55, 53, 128, 130, 133, 54, 17, 33, 11,
                              9, 20, 64, 66, 95, 110, 29, 5, 8, 18, 2, 6, 32, 69, 134, 182, 145, 106, 57, 41, 42, 3, 17, 15, 73,
                              121, 104]  # when including all points
                    # inp_points = plt.ginput(4)
                    # print(inp_points)
                    pm = [item for item in list(range(no_PCL_points)) if item not in al]
                    no_points_to_use = round(occurances['PCL'].size/2)
                    prob = cols[0:no_PCL_points][al] / 10
                    points_to_use = PCL[np.random.default_rng().choice(len(PCL[al, :]), no_points_to_use, replace=False,
                                                          p=np.multiply(prob, 1 / prob.sum(), prob))]
                    PCLal_osim = interpolate_lig_points(points_to_use, osim_points)
                    no_points_to_use = round(occurances['PCL'].size / 2)
                    prob = cols[0:no_PCL_points][pm] / 10
                    points_to_use = PCL[np.random.default_rng().choice(len(PCL[pm, :]), no_points_to_use, replace=False,
                                                         p=np.multiply(prob, 1 / prob.sum(), prob))]
                    PCLpm_osim = interpolate_lig_points(points_to_use, osim_points)

                    if segment == 'femur':
                        no_MCLs_points = no_PCL_points + occ['MCLp'].size
                        osim_points = 6
                        MCLs = ligs.vertices[no_PCL_points:no_MCLs_points]
                        no_points_to_use = occurances['MCLp'].size
                        prob = cols[no_PCL_points:no_MCLs_points][:] / 10
                        MCLs, unq_ind = np.unique(MCLs, axis=0, return_index=True)
                        prob = prob[unq_ind]
                        bla = np.random.default_rng().choice(len(MCLs), no_points_to_use, replace=False,
                                                             p=np.multiply(prob, 1 / prob.sum(), prob))
                        points_to_use = MCLs[bla]

                        MCLs_osim = interpolate_lig_points(points_to_use, osim_points)
                    else:
                        no_MCLs_points = no_PCL_points

                    no_MCLd_points = no_MCLs_points + occ['MCLd'].size  # all points that could
                    osim_points = 5  # points needed for osim model
                    MCLd = ligs.vertices[no_MCLs_points:no_MCLd_points]  # all predicted points in subject that could
                    no_points_to_use = occ['MCLd'].size  # number of points that seem reliable
                    prob = cols[no_MCLs_points:no_MCLd_points][:] / 10
                    points_to_use = MCLd[np.random.default_rng().choice(len(MCLd), size=no_points_to_use, replace=False,
                                                              p=np.multiply(prob, 1 / prob.sum(), prob))]
                    MCLd_osim = interpolate_lig_points(points_to_use, osim_points)

                    no_POL_points = no_MCLd_points + occ['post_obl'].size
                    osim_points = 5
                    post_obl = ligs.vertices[no_MCLd_points:no_POL_points]
                    no_points_to_use = occurances['post_obl'].size
                    prob = cols[no_MCLd_points:no_POL_points][:] / 10
                    points_to_use = post_obl[np.random.default_rng().choice(len(post_obl), no_points_to_use, replace=False, p=np.multiply(prob, 1 / prob.sum(), prob))]
                    post_obl_osim = interpolate_lig_points(points_to_use, osim_points)

                    no_ACL_points = no_POL_points + occ['ACL'].size
                    osim_points = 6
                    ACL = ligs.vertices[no_POL_points:no_ACL_points]
                    if segment == 'femur':
                        # am = [0, 1, 2, 4, 7, 9, 12, 15, 18]
                        am = [64, 71, 60, 66, 40, 42, 41, 69, 65, 68, 61, 62, 51, 29, 27, 26, 56, 77, 30, 22, 18, 12, 49, 48,
                              50, 70, 67, 47, 13, 15, 5, 3, 34, 55, 37, 23, 9, 0, 72]
                    elif segment == 'tibia':
                        # am = [0, 1, 2, 4, 6, 7, 10, 12, 13, 19, 21, 22, 23, 24, 25, 26, 27, 31, 32, 34, 35, 40, 45, 46, 47, 50,
                        #       52, 56, 59, 60, 61, 62, 63]
                        am = [218, 145, 220, 146, 181, 127, 132, 172, 178, 130, 123, 129, 110, 179, 69, 193, 190, 147, 134, 182,
                              141, 219, 142, 217, 137, 215, 216, 144, 148, 151, 189, 188, 191, 195, 183, 171, 133, 177, 128, 125,
                              174, 61, 76, 68, 173, 90, 111, 83, 106, 175, 169, 170, 105, 124, 109, 112, 192, 113, 149, 140, 184,
                              194, 187, 185, 180, 135, 213, 221, 214, 222, 212, 115, 139, 114, 79, 73, 80, 82, 122, 202, 201, 107,
                              56, 38, 51, 29, 20, 21, 27, 57, 45, 19, 17, 41, 39, 4, 11, 50, 28, 24, 78, 207, 209, 199, 143, 138, 136, 186, 150, 131, 126, 176]

                    pl = [item for item in list(range(len(ACL))) if item not in am]
                    # if segment == 'tibia':
                    #     fig2 = plt.figure()
                    #     ax3d = fig2.add_subplot(111, projection='3d')
                    #     ax3d.plot(ACL[:, 0], ACL[:, 1], ACL[:, 2], 'g*', picker=True)
                    #     print(str(len(ACL)/2) + 'points to select')
                    #     def onpick(event):
                    #         ind = event.ind
                    #         print(ind)
                    #     fig2.canvas.mpl_connect('pick_event', onpick)
                    #
                    #     # inp_points = plt.ginput(no_PCL_points/2)
                    #     # print(inp_points)
                    #     fig2 = plt.figure()
                    #     ax3d = fig2.add_subplot(111, projection='3d')
                    #     # ax3d.plot(ACL[:, 0], ACL[:, 1], ACL[:, 2], 'b*')
                    #     ax3d.plot(ACL[am, 0], ACL[am, 1], ACL[am, 2], 'r*')
                    #     ax3d.plot(ACL[pl, 0], ACL[pl, 1], ACL[pl, 2], 'g*')
                    #     plt.show()
                    #     bla
                    no_points_to_use = round(occurances['ACL'].size / 2)
                    prob = cols[0:no_ACL_points][am] / 10
                    points_to_use = ACL[np.random.default_rng().choice(len(ACL[am, :]), no_points_to_use, replace=False,
                                                         p=np.multiply(prob, 1 / prob.sum(), prob))]
                    ACLam_osim = interpolate_lig_points(points_to_use, osim_points)
                    no_points_to_use = round(occurances['ACL'].size / 2)
                    prob = cols[0:no_ACL_points][pl] / 10
                    points_to_use = ACL[np.random.default_rng().choice(len(ACL[pl, :]), no_points_to_use, replace=False,
                                                         p=np.multiply(prob, 1 / prob.sum(), prob))]
                    ACLpl_osim = interpolate_lig_points(points_to_use, osim_points)
                else:
                    no_ACL_points = 0

                if segment != 'tibia':
                    no_LCL_points = no_ACL_points + occ['LCL'].size
                    osim_points = 4
                    LCL = ligs.vertices[no_ACL_points:no_LCL_points]
                    no_points_to_use = occurances['LCL'].size
                    prob = cols[no_ACL_points: no_LCL_points][:] / 10
                    points_to_use = LCL[np.random.default_rng().choice(len(LCL), no_points_to_use, replace=False, p=np.multiply(prob, 1 / prob.sum(), prob))]
                    LCL_osim = interpolate_lig_points(points_to_use, osim_points)

                if segment == 'femur':
                    osim_points_use = np.concatenate([np.asarray(PCLal_osim), np.asarray(PCLpm_osim), np.asarray(MCLs_osim),
                                                        np.asarray(MCLd_osim), np.asarray(post_obl_osim), np.asarray(ACLam_osim),
                                                        np.asarray(ACLpl_osim), np.asarray(LCL_osim)], 1)
                elif segment == 'tibia':
                    osim_points_use = np.concatenate([np.asarray(PCLal_osim), np.asarray(PCLpm_osim),
                                                      np.asarray(MCLd_osim), np.asarray(post_obl_osim), np.asarray(ACLam_osim),
                                                      np.asarray(ACLpl_osim)], 1)
                else:
                    osim_points_use = np.concatenate([np.asarray(LCL_osim)], 1)

                # update ligament attachment in osim file
                if segment == 'femur':
                    no_pathpoint = 0
                else:
                    no_pathpoint = 1
                for i in range(0, len(lig)-1):
                    ligament = opensim.Blankevoort1991Ligament.safeDownCast(forces.get(lig[i]))
                    point = opensim.PathPoint.safeDownCast(ligament.upd_GeometryPath().updPathPointSet().get(no_pathpoint))
                    point.setLocation(opensim.Vec3(osim_points_use[:, i]))

                # scale ligament slack length based on the length of the ligament compared to the generic model
                # it fails if a point is within a wrapping surface (I think) - in that case run scaling MT parameters in Matlab for now
                lig_names = ["PCLpm1", "PCLpm2", "PCLpm3", "PCLpm4", "PCLpm5", "PCLal1", "PCLal2", "PCLal3", "PCLal4", "PCLal5", "MCLd1",
                       "MCLd2", "MCLd3", "MCLd4", "MCLd5", "MCLp1", "MCLp2", "MCLp3", "MCLp4", "MCLp5", "ACLpl1", "ACLpl2",
                       "ACLpl3", "ACLpl4", "ACLpl5", "ACLpl6", "ACLam1", "ACLam2", "ACLam3", "ACLam4", "ACLam5", "ACLam6",
                       "LCL1", "LCL2", "LCL3", "LCL4"]
                model_gen = opensim.Model(osim_model)
                state_gen = model_gen.initSystem()
                scaleFactor = np.zeros([1, len(lig_names)])
                # for i in range(0, len(lig)):
                #     ligament_gen = opensim.Blankevoort1991Ligament.safeDownCast(model_gen.getForceSet().get(lig[i]))
                #     ligament_pers = opensim.Blankevoort1991Ligament.safeDownCast(model.getForceSet().get(lig[i]))
                #     print(lig[i])
                #     ll_gen = ligament_gen.getLength(state_gen)
                #     sl_gen = ligament_gen.get_slack_length()
                #     ll_pers = ligament_pers.getLength(state)
                #
                #     scaleFactor[0, i] = ll_pers / ll_gen
                #     ligament_pers.set_slack_length(sl_gen * scaleFactor[0, i])

                model.printToXML(out_model)

            if run_update_mesh == 1:
                new_bone = out_file

                segment = model.getBodySet().get(body)
                segment.removeGeometry(0)
                new_geometry_path
                segment.attachGeometry(opensim.Mesh(new_geometry_path))

        out_model2 = new_path + r'\lig_' + str(sample) + '_contact.osim'
        if add_contact == 1:
            # add contact to model
            import xml.etree.ElementTree as ET

            tree = ET.parse(r"C:\opensim-jam\opensim-jam-release\examples\walkingS0L\contact.xml")
            root = tree.getroot()
            # Find element to copy
            # member1 = root.find('Smith2018ArticularContactForce')

            tree2 = ET.parse(out_model)
            root2 = tree2.getroot()
            root2.find('Model').find('ForceSet').find('objects').insert(-1, root[0])
            root2.find('Model').find('ForceSet').find('objects').insert(-1, root[1])
            root2.find('Model').find('ContactGeometrySet').find('objects').insert(-1, root[2])
            root2.find('Model').find('ContactGeometrySet').find('objects').insert(-1, root[3])
            root2.find('Model').find('ContactGeometrySet').find('objects').insert(-1, root[4])

            tree2.write(out_model2)

        # create cmd files
        if create_cmd == 1:
            import shutil
            if for_linux == 1:
                main_dir = [r'/home/mariska/Documents/OpenSim/walkingS0L']
                out_model2 = r'/home/mariska/Documents/OpenSim/walkingS0L/S0L_lig_' + str(
                    sample) + '_contact.osim'
            else:
                main_dir = [r'C:\opensim-jam\opensim-jam-release\examples\walkingS0L']
            common_dir = ['results2lig_' + str(sample), 'inputs_' + str(sample)]
            input_files = ['comak_inverse_kinematics_settings2_lig','comak_settings2_lig','joint_mechanics_settings2_lig','inverse_dynamics_settings2_lig','analyze']
            tags = [["model_file", "results_directory", "secondary_constraint_function_file", "results_directory",
                    "output_motion_file", "marker_file"],  # ik
                    ["model_file", "coordinates_file", "results_directory", "settle_sim_results_directory", "external_loads_file", "force_set_file"],  # comak
                    ["model_file", "states_file", "results_directory"],  # joint mechanics
                    ["results_directory", "model_file", "coordinates_file", "external_loads_file"],  # id
                    ["model_file", "results_directory", "states_file"]]  # analyze
            tool_names = ['COMAKInverseKinematicsTool', 'COMAKTool', 'JointMechanicsTool', 'InverseDynamicsTool', 'AnalyzeTool']
            new_var = [[out_model2, main_dir[0]+'/'+common_dir[0]+'/'+'comak-inverse-kinematics', main_dir[0]+'/'+common_dir[0]+'/'+'comak-inverse-kinematics'+'/'+'secondary_coordinate_constraint_functions.xml',
                    main_dir[0]+'/'+common_dir[0]+'/'+'comak-inverse-kinematics', main_dir[0]+'/'+common_dir[0]+'/'+'comak-inverse-kinematics','walk6_ik.mot', '/home/mariska/Documents/OpenSim/walkingS0L/walk7_new_markers.trc'],
                   [out_model2, main_dir[0]+'/'+common_dir[0]+'/'+'comak-inverse-kinematics'+'/'+'walk6_ik.mot', main_dir[0]+'/'+common_dir[0]+'/'+'comak'+'/'+'', main_dir[0]+'/'+common_dir[0]+'/'+'comak'+'/'+'', '/home/mariska/Documents/OpenSim/walkingS0L/inputs/ext_loadsL.xml', '/home/mariska/Documents/OpenSim/walkingS0L/inputs/lenhart2015_reserve_actuatorsL.xml'],
                   [out_model2, main_dir[0]+'/'+common_dir[0]+'/'+'comak/walking_states.sto',main_dir[0]+'/'+common_dir[0]+'/'+'joint-mechanics'],
                   [main_dir[0]+'/'+common_dir[0]+'/'+'inverse-dynamics'+'/'+'',out_model2, main_dir[0]+'/'+common_dir[0]+'/'+'comak'+'/'+'walking_values.sto', '/home/mariska/Documents/OpenSim/walkingS0L/inputs/ext_loadsL.xml'],
                       [out_model2, main_dir[0]+'/'+common_dir[0]+'/'+'comak', main_dir[0]+'/'+common_dir[0]+'/'+'comak/walking_states.sto']]
            for dir1 in main_dir:
                for dir2 in common_dir:
                    try:
                        os.makedirs(os.path.join(dir1, dir2))
                    except OSError:
                        pass
                    if for_linux == 1:
                        try:
                            os.makedirs(os.path.join(r'C:\opensim-jam\opensim-jam-release\examples\walkingS0L', 'linux', dir2))
                        except OSError:
                            pass
                    if dir2 == 'inputs_' + str(sample):
                        for ind, file in enumerate(input_files):
                            if file == 'analyze':
                                if for_linux == 1:
                                    original = os.path.join(r'C:\opensim-jam\opensim-jam-release\examples\walkingS0L', 'inputs', file + '.xml')
                                    target = os.path.join(r'C:\opensim-jam\opensim-jam-release\examples\walkingS0L', 'linux', dir2, file + '.xml')
                                    shutil.copyfile(original, target)
                                else:
                                    original = os.path.join(dir1,'inputs', file + '.xml')
                                    if for_linux == 1:
                                        target = os.path.join(dir1, 'linux', dir2, file + '.xml')
                                    else:
                                        target = os.path.join(dir1, dir2, file + '.xml')
                                    shutil.copyfile(original, target)

                                adapt_setup(target,tags[ind], new_var[ind], tool_names[ind])
                        if for_linux == 1:
                            run_file = open(os.path.join(r'C:\opensim-jam\opensim-jam-release\examples\walkingS0L', 'linux', 'run_walking2_lig_' + str(sample) + '.sh'), mode='w')

                            # run_file.writelines(r"/opt/opensim-core-jam/bin/opensim-cmd run-tool " + dir1 + "/" + dir2 + r"/comak_inverse_kinematics_settings2_lig.xml")
                            # run_file.writelines("\n")
                            # run_file.writelines(r"mv out.log " + dir1 + "/" + dir2 + r"/comak-inverse-kinematics_out.log")
                            # run_file.writelines("\n")
                            # run_file.writelines(r"mv err.log " + dir1 + "/" + dir2 + r"/comak-inverse-kinematics_err.log")
                            # run_file.writelines("\n")
                            run_file.writelines(r"/opt/opensim-core-jam/bin/opensim-cmd run-tool " + dir1 + "/" + dir2 + r"/comak_settings2_lig.xml")
                            run_file.writelines("\n")
                            run_file.writelines(r"mv out.log " + dir1 + "/" + dir2 + r"/comak_out.log")
                            run_file.writelines("\n")
                            run_file.writelines(r"mv err.log " + dir1 + "/" + dir2 + r"/comak_err.log")
                            run_file.writelines("\n")
                            # run_file.writelines(r"/opt/opensim-core-jam/bin/opensim-cmd run-tool " + dir1 + "/" + dir2 + r"/joint_mechanics_settings2_lig.xml")
                            # run_file.writelines("\n")
                            # run_file.writelines(r"mv out.log " + dir1 + "/" + dir2 + r"/joint_mechanics_out.log")
                            # run_file.writelines("\n")
                            # run_file.writelines(r"mv err.log " + dir1 + "/" + dir2 + r"/joint_mechanics_err.log")
                            # run_file.writelines("\n")
                            run_file.writelines(r"/opt/opensim-core-jam/bin/opensim-cmd run-tool " + dir1 + "/" + dir2 + r"/inverse_dynamics_settings2_lig.xml")
                            run_file.writelines("\n")
                            run_file.writelines(r"mv out.log " + dir1 + "/" + dir2 + r"/inverse_dynamics_out.log")
                            run_file.writelines("\n")
                            run_file.writelines(r"mv err.log " + dir1 + "/" + dir2 + r"/inverse_dynamics_out.log")

                            # cmd_files = open(r"C:\opensim-jam\opensim-jam-release\examples\walkingS0L\linux\run_walking2_ligs.bash",
                            #     mode="a")
                            # cmd_files.writelines(r'sed - i \'s/\r//g' 'run_walking2_lig_' + str(sample) + '.sh')
                            # cmd_files.writelines("\n")
                            # cmd_files.writelines('sh ' + dir1 +'/'+'run_walking2_lig_' + str(sample) + '.sh')
                            # cmd_files.writelines("\n")
                        else:
                            run_file = open(os.path.join(dir1,'run_walking2_lig_' + str(sample) + '.cmd'), mode='w')

                            run_file.writelines(r"set BIN=C:\opensim-jam\opensim-jam-release\bin")
                            run_file.writelines("\n")
                            run_file.writelines(r"set OPENSIM=C:\opensim-jam\opensim-jam-release\opensim")
                            run_file.writelines("\n")
                            run_file.writelines(r"set PATH=%BIN%;%OPENSIM%;%PATH%")
                            # run_file.writelines("\n")
                            # run_file.writelines(r"%BIN%\comak-inverse-kinematics %BIN%\jam_plugin.dll " + dir1 + "/" + dir2 + r"\comak_inverse_kinematics_settings2_lig.xml")
                            # run_file.writelines("\n")
                            # run_file.writelines(r"move out.log " + dir1 + "/" + dir2 + r"\comak-inverse-kinematics_out.log")
                            # run_file.writelines("\n")
                            # run_file.writelines(r"move err.log " + dir1 + "/" + dir2 + r"\comak-inverse-kinematics_err.log")
                            # run_file.writelines("\n")
                            # run_file.writelines(r"%BIN%\comak %BIN%\jam_plugin.dll " + dir1 + "/" + dir2 + r"\comak_settings2_lig.xml")
                            # run_file.writelines("\n")
                            # run_file.writelines(r"move out.log " + dir1 + "/" + dir2 + r"\comak_out.log")
                            # run_file.writelines("\n")
                            # run_file.writelines(r"move err.log " + dir1 + "/" + dir2 + r"\comak_err.log")
                            # run_file.writelines("\n")
                            # run_file.writelines(r"%BIN%\joint-mechanics %BIN%\jam_plugin.dll " + dir1 + "/" + dir2 + r"\joint_mechanics_settings2_lig.xml")
                            # run_file.writelines("\n")
                            # run_file.writelines(r"move out.log " + dir1 + "/" + dir2 + r"\joint_mechanics_out.log")
                            # run_file.writelines("\n")
                            # run_file.writelines(r"move err.log " + dir1 + "/" + dir2 + r"\joint_mechanics_err.log")
                            # run_file.writelines("\n")
                            # run_file.writelines(r"%OPENSIM%\opensim-cmd -L %BIN%\jam_plugin.dll run-tool " + dir1 + "/" + dir2 + r"\inverse_dynamics_settings2_lig.xml")
                            # run_file.writelines("\n")
                            # run_file.writelines(r"move out.log " + dir1 + "/" + dir2 + r"\inverse_dynamics_out.log")
                            # run_file.writelines("\n")
                            # run_file.writelines(r"move err.log " + dir1 + "/" + dir2 + r"\inverse_dynamics_out.log")
                            run_file.writelines("\n")
                            run_file.writelines(
                                r"%OPENSIM%\opensim-cmd -L %BIN%\jam_plugin.dll run-tool " + dir1 + "/" + dir2 + r"\analyze.xml")
                            run_file.writelines("\n")
                            run_file.writelines(r"move out.log " + dir1 + "/" + dir2 + r"\analyze_out.log")
                            run_file.writelines("\n")
                            run_file.writelines(r"move err.log " + dir1 + "/" + dir2 + r"\analyze_out.log")

                            # cmd_files = open(r"C:\opensim-jam\opensim-jam-release\examples\walkingS0L\run_walking2_ligs.cmd", mode="a")
                            # cmd_files.writelines('call ' + os.path.join(dir1, 'run_walking2_lig_' + str(sample) + '.cmd'))
                            # cmd_files.writelines("\n")
