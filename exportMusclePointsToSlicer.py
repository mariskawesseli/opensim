import trimesh
import numpy as np
from scipy import interpolate
import os
import xml.etree.ElementTree as ET
import glob
import sys
import re

# sys.path.remove(r'C:\\OpenSim 4.3\\plugins')
# paths_to_remove = [r'C:\OpenSim 4.3\bin', r'C:\OpenSim 4.3\plugins', r'C:\OpenSim 4.3\sdk\Python']
# paths = os.environ["PATH"].split(os.pathsep) # split the PATH variable into a list of paths
# paths = [path for path in paths if path not in paths_to_remove] # remove the paths you want to remove
# new_path = os.pathsep.join(paths) # join the paths back into a single string separated by the OS path separator
# # os.environ["PATH"] = new_path # set the updated PATH variable
os.environ['PATH'] = r"C:\OpenSim 4.1\bin" + os.pathsep + os.environ['PATH']
import opensim

opensim.LoadOpenSimLibrary('C:\OpenSim 4.1\plugins\jam_plugin')

# read osim model
# read muscle points
# convert muscle points to mri ref frame - fit segmented bone to generic bone
# write converted muscle points to file slicer can read

data_folder = r'C:\Users\User\OneDrive - Delft University of Technology\Documents\OpenSim\KOALA\biomechanics\OpenSim\gait2392'
gen_model_file = r'LPACL05.osim'
geometry_folder = r'C:\Users\User\OneDrive - Delft University of Technology\Documents\OpenSim\KOALA\biomechanics\OpenSim'

gen_model = opensim.Model(os.path.join(data_folder, gen_model_file))
gen_state = gen_model.initSystem()

pers_bone_files = []
seg_to_pers = ['pelvis','femur_l', 'tibia_l']

scale_matrix = np.eye(4)
scale_matrix[:3, :3] *= 1000

pattern = r"\[([-+]?\d*\.\d+|\d+),\s*([-+]?\d*\.\d+|\d+),\s*([-+]?\d*\.\d+|\d+)\]"
muscle_points = []
muscle_point_names = []
nbr_muscles = gen_model.getMuscles().getSize()
for m in range(0, nbr_muscles):
    nbr_points = gen_model.getMuscles().get(m).getGeometryPath().getPathPointSet().getSize()
    for p in range(0, nbr_points):
        body = gen_model.getMuscles().get(m).getGeometryPath().getPathPointSet().get(p).getBodyName()
        if body in seg_to_pers:
            temp = gen_model.getMuscles().get(m).getGeometryPath().getPathPointSet().get(p).getLocation(
                gen_state).toString()
            match = re.search(pattern, temp)
            muscle_point_os = ([float(num) for num in match.groups()])

            Tfull = np.loadtxt(os.path.join(data_folder, os.path.splitext(body)[0] + '_Tmri.txt'))

            transformed_point = trimesh.transform_points(np.array(muscle_point_os).reshape(1, -1),
                                                         trimesh.transformations.inverse_matrix(Tfull))[0]
            transformed_point = trimesh.transform_points(transformed_point.reshape(1, -1), scale_matrix)[0]
            muscle_points.append(transformed_point)
            muscle_point_names.append(
                gen_model.getMuscles().get(m).getGeometryPath().getPathPointSet().get(p).getName())

# pers_bone_files = ['Femur.stl', 'Tibia.stl']
# for bone_file, body in zip(pers_bone_files, seg_to_pers):
#     Tfull = np.loadtxt(os.path.join(data_folder, os.path.splitext(body)[0] + '_Tmri.txt'))
#     bone_file_new = f"{os.path.splitext(bone_file)[0]}{'_os'}{os.path.splitext(bone_file)[1]}"
#     new_geometry_path = os.path.join(geometry_folder, bone_file_new)
#     mesh2 = trimesh.load_mesh(os.path.join(geometry_folder, bone_file_new))
#
#     icp = np.loadtxt(os.path.join(data_folder, os.path.splitext(body)[0] + '_Tmri_icp.txt'))
#     T = np.loadtxt(os.path.join(data_folder, os.path.splitext(body)[0] + '_Tmri_T.txt'))
#     R = np.loadtxt(os.path.join(data_folder, os.path.splitext(body)[0] + '_Tmri_R.txt'))
#
#     mesh2.apply_transform(trimesh.transformations.inverse_matrix(icp))
#     mesh2.apply_transform(trimesh.transformations.inverse_matrix(T))
#     mesh2.apply_transform(trimesh.transformations.inverse_matrix(R))
#     mesh2.apply_transform(scale_matrix)
#
#     # mesh2.apply_transform(scale_matrix)
#     # mesh2.apply_transform(Tfull)
#     mesh2.export(geometry_folder + '/' + f"{os.path.splitext(bone_file)[0]}{'_test'}{os.path.splitext(bone_file)[1]}")

muscle_points = np.asarray(muscle_points)
data_to_write = np.c_[muscle_point_names, -1 * muscle_points[:, 0], -1 * muscle_points[:, 1], muscle_points[:, 2]]

# Save the muscle points in a format that 3D Slicer can read
gen_model_muscles = os.path.splitext(gen_model_file)[0] + '_musclepoints2.fcsv'
np.savetxt(os.path.join(data_folder, gen_model_muscles), data_to_write, delimiter=",", fmt='%s')
