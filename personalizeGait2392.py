import trimesh
import numpy as np
from scipy import interpolate
# import matplotlib.pyplot as plt
import os
import xml.etree.ElementTree as ET
import glob
import sys
import pyvista as pv

# sys.path.remove(r'C:\\OpenSim 4.3\\plugins')
# paths_to_remove = [r'C:\OpenSim 4.3\bin', r'C:\OpenSim 4.3\plugins', r'C:\OpenSim 4.3\sdk\Python']
# paths = os.environ["PATH"].split(os.pathsep) # split the PATH variable into a list of paths
# paths = [path for path in paths if path not in paths_to_remove] # remove the paths you want to remove
# new_path = os.pathsep.join(paths) # join the paths back into a single string separated by the OS path separator
# # os.environ["PATH"] = new_path # set the updated PATH variable
os.environ['PATH'] = r"C:\OpenSim 4.1\bin" + os.pathsep + os.environ['PATH']
import opensim
opensim.LoadOpenSimLibrary('C:\OpenSim 4.1\plugins\jam_plugin')
# import matplotlib
# matplotlib.use('TkAgg')

model_folder = r'C:\Users\mariskawesseli\Documents\OpenSim\KOALA\biomechanics\OpenSim\gait2392'
gen_model = r'LPACL05.osim'
pers_model = r'LPACL05_personalized.osim'

gen_model = opensim.Model(os.path.join(model_folder,gen_model))
gen_state = gen_model.initSystem()

geometry_folder = r'C:\Users\mariskawesseli\Documents\OpenSim\KOALA\biomechanics\OpenSim'
pers_bone_files = ['Femur.stl', 'Tibia.stl']
pers_bone_parts = ['distal', 'proximal']
seg_to_pers = ['femur_l', 'tibia_l']
opensim_geometry_folder = r'C:\OpenSim 4.1\Geometry'
opensim_meshes = ['femur_l.vtp', 'tibia_l.vtp']

personalize_bones = 1
personalize_muscles = 0

if personalize_bones == 1:
    # check that pers_bone_files and seg_pers are same length
    if len(pers_bone_files) == len(seg_to_pers):
        for body, bone_file, os_bone, part in zip(seg_to_pers, pers_bone_files, opensim_meshes, pers_bone_parts):
            # Load the original mesh
            mesh = pv.read(os.path.join(geometry_folder, bone_file))

            # Simplify the mesh
            simplified_mesh = mesh.decimate(target_reduction=0.9)
            simplified_mesh = simplified_mesh.scale(0.001)

            # Save the simplified mesh to a new file
            bone_file_new = f"{os.path.splitext(bone_file)[0]}{'_os'}{os.path.splitext(bone_file)[1]}"
            simplified_mesh.save(os.path.join(geometry_folder, bone_file_new))

            # set orientation of bones correct - fit to generic file?
            # convert opensim vtp file to stl
            opensim_mesh = os.path.join(opensim_geometry_folder, os_bone)
            mesh = pv.read(opensim_mesh)
            opensim_mesh_stl = os.path.join(opensim_geometry_folder, os.path.splitext(os_bone)[0] + '.stl')
            mesh.save(opensim_mesh_stl)
            mesh1 = trimesh.load_mesh(opensim_mesh_stl)
            # mesh segmented from MRI
            mesh2 = trimesh.load_mesh(os.path.join(geometry_folder, bone_file_new))


            xaxis, yaxis, zaxis = [1, 0, 0], [0, 1, 0], [0, 0, 1]

            # scale generic mesh for subject
            scale_factor = gen_model.getBodySet().get(body).get_attached_geometry(0).get_scale_factors()
            origin = np.array((0,0,0))
            S = trimesh.transformations.scale_matrix(scale_factor.get(0), origin, xaxis)
            S1 = trimesh.transformations.scale_matrix(scale_factor.get(1), origin, yaxis)
            S2 = trimesh.transformations.scale_matrix(scale_factor.get(2), origin, zaxis)
            mesh1.apply_transform(trimesh.transformations.concatenate_matrices(S, S1, S2))

            # Rotate segmented bone (depend on orientation of the MRI)
            Rx = trimesh.transformations.rotation_matrix(-90 / (180 / np.pi), xaxis)
            Ry = trimesh.transformations.rotation_matrix(-90 / (180 / np.pi), yaxis)
            R = trimesh.transformations.concatenate_matrices(Ry, Rx)
            mesh2.apply_transform(R)

            # Translate segmented mesh to OpenSim bone location
            if part == 'distal':
                trans = [mesh1.bounds[0][0] - mesh2.bounds[0][0], mesh1.bounds[0][1] - mesh2.bounds[0][1],
                         mesh1.bounds[1][2] - mesh2.bounds[1][2]]
                T = trimesh.transformations.translation_matrix(trans)
            elif part == 'proximal':
                T = trimesh.transformations.translation_matrix(mesh1.bounds[1] - mesh2.bounds[1])
            mesh2.apply_transform(T)
            # mesh2.export(os.path.join(geometry_folder, bone_file_new))  # export translated file

            kwargs = {"scale": False}
            icp, mesh2T, cost = trimesh.registration.icp(mesh2.vertices, mesh1, initial=np.identity(4), threshold=1e-2,
                                                         max_iterations=20, **kwargs)
            mesh2.apply_transform(icp)
            mesh2.export(os.path.join(geometry_folder, bone_file_new))

            bone_file_new2 = f"{os.path.splitext(bone_file)[0]}{'_testt2'}{os.path.splitext(bone_file)[1]}"

            Tfull = trimesh.transformations.concatenate_matrices(icp,T, R)
            np.savetxt(os.path.join(model_folder, os.path.splitext(os_bone)[0] + '_Tmri.txt'), Tfull)
            # scale_matrix = np.eye(4)
            # scale_matrix[:3, :3] *= 1000
            # mesh2.apply_transform(trimesh.transformations.inverse_matrix(Tfull))
            # mesh2.apply_transform(scale_matrix)
            # mesh2.export(os.path.join(geometry_folder, bone_file_new2))
            #
            # np.savetxt(os.path.join(model_folder, os.path.splitext(os_bone)[0] + '_Tmri_icp.txt'), icp)
            # np.savetxt(os.path.join(model_folder, os.path.splitext(os_bone)[0] + '_Tmri_T.txt'), T)
            # np.savetxt(os.path.join(model_folder, os.path.splitext(os_bone)[0] + '_Tmri_R.txt'), R)

            # export muscles?

            # scale joint kinematics?

            segment = gen_model.getBodySet().get(body)
            # segment.removeGeometry(0)
            new_geometry_path = os.path.join(geometry_folder, bone_file_new)
            segment.attachGeometry(opensim.Mesh(new_geometry_path))

            gen_model.printToXML(os.path.join(model_folder, pers_model))
    else:
        print("The number of bone files is not the same as the number of segments to personalize. "
              "Provide one bone file per segment")

