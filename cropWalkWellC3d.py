import ezc3d
import numpy as np

def crop_c3d_to_steps(input_file, output_prefix, steps_to_save=20, threshold=10):
    c3d = ezc3d.c3d(input_file)
    grf = c3d["data"]["points"][2, :, :]  # Assuming Z is at index 2
    left_belt = grf[0]  # Assuming channel 0 is left belt
    right_belt = grf[1]  # Assuming channel 1 is right belt

    # Detect steps using threshold
    left_steps = np.where(left_belt > threshold)[0]
    right_steps = np.where(right_belt > threshold)[0]

    # Match the step indices for left and right belts
    steps = min(len(left_steps), len(right_steps))
    if steps < steps_to_save:
        print(f"Not enough steps detected ({steps}).")

    # Crop to the last 'steps_to_save' steps
    cropped_left_steps = left_steps[-steps_to_save:]
    cropped_right_steps = right_steps[-steps_to_save:]

    # Save individual steps to new files
    for i, (l_step, r_step) in enumerate(zip(cropped_left_steps, cropped_right_steps)):
        step_data = c3d.copy()
        step_data["data"]["points"] = c3d["data"]["points"][:, :, l_step:r_step]

        output_file = f"{output_prefix}_step_{i+1}.c3d"
        ezc3d.c3d(output_file, step_data)
        print(f"Saved step {i+1} to {output_file}")

    # Check for steps on the wrong belt
    if any(left_belt[right_steps] > threshold) or any(right_belt[left_steps] > threshold):
        print("Warning: Steps detected on the wrong belt!")

# Usage
crop_c3d_to_steps("input_file.c3d", "output_prefix", steps_to_save=20, threshold=10)
