import os
import opensim as osim
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib
import csv
matplotlib.use("TkAgg")

def osimTableFromDataframe(df, labels, time):  # not working
    timeseriesosimtable = osim.TimeSeriesTableVec3()
    # Set the TimesSeriesTable() column names
    osimlabels = osim.StdVectorString()
    nfields = len(df.columns)
    nRows = len(df)
    # for i in range(0,nfields):
    #     osimlabels.add(labels[i])
    # timeseriesosimtable.setColumnLabels(osimlabels)

    # Build the TimesSeriesTable()
    # Get an OpenSim Row Vector
    row = osim.RowVectorVec3(nfields)
    for iRow in range(0,nRows):
        # Create and fill a row of data
        for iCol in range(0,nfields):
            # Make a vec3 element from the rowdata
            row.setTo(iCol-1, osim.osimVec3FromArray(df[labels[iCol]][iRow,:]))
        # Append the RowVectorofVec3's to the opensim table

    timeseriesosimtable.appendRow(iRow-1, row)
    timeColumn = timeseriesosimtable.getIndependentColumn()
    for i in range(0,nRows):
        timeColumn.set(i, time(i))

    return timeseriesosimtable


path = r"C:\Users\mariskawesseli\Documents\MOBI\data\2021-11-03-Mariska_PatellaStudy\Data"
csv_file = r"walk7_analog.csv"  # in Mokka export analog channels as ASCII file
output_file = 'walk7'
MVC = 1  # normalize EMG to maximum in trial

# Get the path to a csv file
csv_path = os.path.join(path, csv_file)

# Get analog data and sample frequency
analog_data = pd.read_csv(csv_path, header=4, encoding='cp1252', skiprows=[6])
with open(csv_path, newline='') as f:
    reader = csv.reader(f)
    for i in range(4):
        row = next(reader)
fs = int(row[1])

# Read EMG data from c3d file
channels_names = ['vast lat', 'rect fem', 'vast med', 'biceps fem', 'semiten', 'tib ant', 'gastroc lat', 'gastroc med', 'sol']
channels_used = [11, 5, 6, 10, 15, 8, 13, 14, 16]

# Filter properties
order = 4
cutoff_band = np.array([20, 400])
cutoff_low = 10

# Window to calculate MVC
window = 0.01  # 0.25*0.5

# Get used channels
EMG_raw = analog_data[['CH' + str(sub) for sub in channels_used]]
col_rename_dict = {i:j for i,j in zip(['CH' + str(sub) for sub in channels_used], channels_names)}
EMG_raw.rename(columns=col_rename_dict, inplace=True)
# EMG_raw.plot(subplots=True)

# band pass filter
EMG_band = pd.DataFrame()
b, a = signal.butter(order, cutoff_band / (fs / 2), btype='band', analog=False, output='ba', fs=None)
for col in EMG_raw:
    EMG_band[col] = signal.filtfilt(b, a, EMG_raw[col])
# EMG_band.plot(subplots=True)

# Rectify
EMG_rectified = abs(EMG_band)
# EMG_rectified.plot(subplots=True)

# Low-pass filter
EMG_low = pd.DataFrame()
b, a = signal.butter(order, cutoff_low / (fs / 2), btype='lowpass')
for col in EMG_raw:
    EMG_low[col] = signal.filtfilt(b, a, EMG_rectified[col])
# EMG_low.plot(subplots=True)

if MVC == 1:
    window_length = round(window*fs)
    RMS_norm_Low = np.zeros((len(EMG_low),len(EMG_low.columns)))
    EMG_sq = EMG_low * EMG_low
    EMG_sq_np = EMG_sq.to_numpy()

    for i in range(window_length, len(EMG_low)-(window_length)):
        for j in range(0, len(EMG_low.columns)):
            point_sel = i
            RMS_norm_Low[point_sel,j] = np.sqrt(np.mean(EMG_sq_np[i-window_length:i+window_length,j]))
    MVC = np.max(RMS_norm_Low,0)
    EMG_norm = EMG_low.to_numpy()/MVC[:, None].T
    EMG_norm_df = pd.DataFrame(EMG_norm,columns=[sub + '_NORM' for sub in channels_names])
    # EMG_norm_df.plot(subplots=True)
    EMGtable = pd.concat([EMG_low, EMG_norm_df], axis=1, join='inner')
else:
    EMGtable = EMG_low


# Write data to csv file
EMGtable.insert(0, "Time", analog_data['Time'].to_numpy(), allow_duplicates=True)
EMGtable.to_csv(os.path.join(path, output_file + '_EMG.csv'), index=False)
