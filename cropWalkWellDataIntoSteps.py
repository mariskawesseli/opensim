import numpy as np
import pandas as pd
import os
import opensim as osim


def load_trc(file_path):
    """Load TRC file into a pandas DataFrame."""
    with open(file_path, 'r') as file:
        lines = file.readlines()
    header = lines[:5]
    column_names = lines[5].strip().split("\t")
    data = pd.read_csv(file_path, skiprows=4, sep="\t")
    time = data['Unnamed: 1'].values
    marker_data = data.iloc[:, 2:-1].values  # Exclude Time column
    return time, marker_data, header, column_names


def load_mot(file_path):
    """Load MOT file into a pandas DataFrame."""
    with open(file_path, 'r') as file:
        lines = file.readlines()
    header = lines[:5]
    column_names = lines[5].strip().split("\t")
    data = pd.read_csv(file_path, skiprows=5, sep="\t")
    time = data['time'].values
    grf_data = data.iloc[:, 1:].values  # Exclude time column
    return time, grf_data, header, column_names


def save_trc(file_path, time, marker_data, header, column_names, frame_numbers):
    """Save cropped data to a TRC file."""
    with open(file_path, 'w') as file:
        file.writelines(header)
        file.write("\t".join(column_names) + "\n")
        for f, t, row in zip(frame_numbers, time, marker_data):
            file.write(f"{f}\t" + f"{t}\t" + "\t".join(map(str, row)) + "\n")


def save_mot(file_path, time, grf_data, header, column_names):
    """Save cropped data to a MOT file."""
    with open(file_path, 'w') as file:
        file.writelines(header)
        file.write("time\t" + "\t".join(column_names[1:]) + "\n")
        for t, row in zip(time, grf_data):
            file.write(f"{t}\t" + "\t".join(map(str, row)) + "\n")


trial_info = pd.read_excel(r"C:\Users\User\Documents\WalkWell\data\WalkWell\Trial_info.xlsx")
mocap_path = r'C:\Users\User\Documents\WalkWell\data\WalkWell'

for i, pt in enumerate(trial_info["PP"]):
    participant = pt
    BW = trial_info['BW'][i]
    foot = trial_info['foot'][i]
    test_leg = True
    trial = trial_info['mocap_trial'][i]
    path = os.path.join(mocap_path, pt)

    newpath = os.path.join(path, 'opensim')

    output_file = trial

    # Load TRC and MOT files
    trc_file = os.path.join(newpath, output_file + '_markers.trc')
    mot_file = os.path.join(newpath, output_file + '_forces.mot')
    threshold = 50
    last_n_steps = 20

    time_trc, marker_data, trc_header, trc_columns = load_trc(trc_file)
    time_mot, grf_data, grf_header, grf_columns = load_mot(mot_file)

    # Ensure time vectors align
    time_mot_indices = np.searchsorted(time_mot, time_trc)
    valid_indices = time_mot_indices < len(time_mot)  # Ensure indices are within bounds
    aligned_grf_data = grf_data[time_mot_indices[valid_indices], :]
    aligned_time = time_trc[valid_indices]
    aligned_marker_data = marker_data[valid_indices, :]

    # Detect steps based on GRF threshold - belt 1 only
    # grf_magnitude = np.linalg.norm(grf_data[:,9:12], axis=1)  # belt 2
    grf_magnitude = np.linalg.norm(grf_data[:, 0:4], axis=1)
    step_indices = np.where(grf_magnitude > threshold)[0]
    step_starts_all = np.split(step_indices, np.where(np.diff(step_indices) > 1)[0] + 1)
    step_starts = step_starts_all[-last_n_steps:]  # Use last N steps

    # Crop and save each step
    for i, step in enumerate(step_starts):
        if i == len(step_starts)-1:
            start, end = step[0], step[-1]
        else:
            start, end = step[0], step_starts[i+1][0]  #step[-1]
        cropped_time = time_trc[int(start/10):int(end/10) + 1]
        cropped_time_grf = time_mot[start:end + 1]
        cropped_markers = marker_data[int(start/10):int(end/10) + 100, :]
        cropped_grf = grf_data[start:end + 1, :]
        trc_frames = np.arange(1,len(cropped_markers)+1)

        save_trc(os.path.join(newpath, f"{output_file}_step_{i + 1}.trc"), cropped_time, cropped_markers, trc_header, trc_columns, trc_frames)
        save_mot(os.path.join(newpath, f"{output_file}_step_{i + 1}.mot"), cropped_time_grf, cropped_grf, grf_header, grf_columns)

