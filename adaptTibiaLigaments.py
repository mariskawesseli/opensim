import trimesh
import numpy as np
from scipy import interpolate
import matplotlib.pyplot as plt
import os
import xml.etree.ElementTree as ET

os.environ['PATH'] = r"C:\OpenSim 4.3\bin" + os.pathsep + os.environ['PATH']
import opensim
import matplotlib
matplotlib.use('TkAgg')


def interpolate_lig_points(lig_points, no_points, plot=0):
    from scipy.stats import gaussian_kde
    x = lig_points[:, 0]
    y = lig_points[:, 1]
    z = lig_points[:, 2]
    goon = 1
    n = 0
    # Create a uniformly spaced grid
    while goon == 1:
        steps = no_points / 2 + n  # number of rows and columns for the grid
        grid_steps = complex(str(steps) + 'j')

        interp = interpolate.Rbf(x, y, z, function='thin_plate')
        yi, xi = np.mgrid[min(lig_points[:, 1]):max(lig_points[:, 1]):grid_steps,
                 min(lig_points[:, 0]):max(lig_points[:, 0]):grid_steps]
        zi = interp(xi, yi)

        inds_remove = []
        inds_nan = []
        diff_val = []
        xi_nan = xi
        yi_nan = yi
        zi_nan = zi
        for i in range(0, len(xi)):
            for j in range(0, len(xi)):
                diff = np.linalg.norm(lig_points[:, :] - np.asarray([xi[i, j], yi[i, j], zi[i, j]]), axis=1)
                diff_val.append(np.abs(np.amin(diff)))
                if np.amin(diff) > 2:
                    inds_remove.append([i * steps + j])
                    inds_nan.append([i, j])
                    xi_nan[i, j] = np.nan
                    yi_nan[i, j] = np.nan
                    zi_nan[i, j] = np.nan
        n = n + 1
        if np.count_nonzero(~np.isnan(xi_nan)) >= no_points or n == 100:
            # print(str(np.count_nonzero(~np.isnan(xi_nan))) + ' ' + str(no_points))
            goon = 0
        diff_val = np.zeros([len(xi_nan), len(xi_nan)])
        if np.count_nonzero(~np.isnan(xi_nan)) > no_points:
            to_remove = np.count_nonzero(~np.isnan(xi_nan)) - no_points
            for i in range(0, len(xi_nan)):
                for j in range(0, len(xi_nan)):
                    diff = np.linalg.norm(lig_points[:, :] - np.asarray([xi_nan[i, j], yi_nan[i, j], zi_nan[i, j]]),
                                          axis=1)
                    diff_val[i, j] = np.abs(np.amin(diff))
            for k in range(0, to_remove):
                i, j = np.unravel_index(np.nanargmax(diff_val), diff_val.shape)
                diff_val[i, j] = np.nan
                xi_nan[i, j] = np.nan
                yi_nan[i, j] = np.nan
                zi_nan[i, j] = np.nan
        # print('-1 ' + str(np.count_nonzero(~np.isnan(xi_nan))) + ' ' + str(no_points))

    if len(lig_points) == 2:
        tck, u = interpolate.splprep([lig_points[:, 0], lig_points[:, 1], lig_points[:, 2]], s=10, k=1)
        x_knots, y_knots, z_knots = interpolate.splev(tck[0], tck)
        u_fine = np.linspace(0, 1, no_points)
        x_fine, y_fine, z_fine = interpolate.splev(u_fine, tck, der=0)
        # lig_points_osim = np.transpose(np.asarray([x_fine, y_fine, z_fine]))
        xi_nan, yi_nan, zi_nan = x_fine, y_fine, z_fine

    lig_points_osim = xi_nan[np.logical_not(np.isnan(xi_nan))] / 1000, yi_nan[np.logical_not(np.isnan(yi_nan))] / 1000, \
                      zi_nan[np.logical_not(np.isnan(zi_nan))] / 1000
    lig_points_osim = np.asarray(lig_points_osim)
    i = np.argsort(lig_points_osim[0, :])
    lig_points_osim = lig_points_osim[:, i]
    if plot == 1:
        fig2 = plt.figure()
        plt.ioff()
        ax3d = fig2.add_subplot(111, projection='3d')
        ax3d.plot(lig_points[:, 0], lig_points[:, 1], lig_points[:, 2], 'r*')
        # ax3d.plot(x_knots, y_knots, z_knots, 'bo')
        # ax3d.plot(x_fine, y_fine, z_fine, 'go')
        ax3d.scatter(xi_nan[:], yi_nan[:], zi_nan[:], c='g')
        # fig2.show()
        plt.show()

    return lig_points_osim


def adapt_setup(file, tags, input, tool):
    if tool == 'COMAKInverseKinematicsTool':
        tags_ik = tags[3:]
        input_ik = input[3:]
        tags = tags[0:3]
        input = input[0:3]

    tree = ET.parse(file)
    root = tree.getroot()
    if tool == 'COMAKInverseKinematicsTool':
        for i, tag in enumerate(tags_ik):
            elem = root.find(tool).find('InverseKinematicsTool').find(tag)
            elem.text = input_ik[i]
    for i, tag in enumerate(tags):
        elem = root.find(tool).find(tag)
        elem.text = input[i]

    tree.write(file)


# TODO currently need to remove the contact from osim file as this is not recognized
subjects = ['S0']  # [9,13,19,23,26,29,32,35,37,41]
sides = ['L']
segments = ['tibia']

run_fit = 0
run_find_points = 0
create_cmd = 1
add_contact = 0
for_linux = 1

lig_femur = ["PCLpm1", "PCLpm2", "PCLpm3", "PCLpm4", "PCLpm5", "PCLal1", "PCLal2", "PCLal3", "PCLal4", "PCLal5",
             "MCLs1", "MCLs2", "MCLs3", "MCLs4", "MCLs5", "MCLs6", "MCLd1", "MCLd2", "MCLd3", "MCLd4", "MCLd5", "MCLp1",
             "MCLp2", "MCLp3","MCLp4", "MCLp5", "ACLpl1", "ACLpl2", "ACLpl3", "ACLpl4", "ACLpl5", "ACLpl6", "ACLam1",
             "ACLam2", "ACLam3", "ACLam4", "ACLam5", "ACLam6", "LCL1", "LCL2", "LCL3", "LCL4"]
lig_tibia = ["PCLpm1", "PCLpm2", "PCLpm3", "PCLpm4", "PCLpm5", "PCLal1", "PCLal2", "PCLal3", "PCLal4", "PCLal5",
             "MCLd1", "MCLd2", "MCLd3", "MCLd4", "MCLd5", "MCLp1", "MCLp2", "MCLp3", "MCLp4", "MCLp5", "ACLpl1",
             "ACLpl2", "ACLpl3", "ACLpl4", "ACLpl5", "ACLpl6", "ACLam1", "ACLam2", "ACLam3", "ACLam4", "ACLam5",
             "ACLam6"]
lig_fibula = ["LCL1", "LCL2", "LCL3", "LCL4"]


out_model = r'C:\Users\mariskawesseli\Documents\OpenSim\4DCT\Patient001\lenhart2015.osim'
in_model = r'C:\Users\mariskawesseli\Documents\OpenSim\4DCT\Patient001\lenhart2015 - Copy.osim'

osim_model = in_model
model = opensim.Model(osim_model)
state = model.initSystem()
forces = model.getForceSet()

for seg_ind, segment in enumerate(segments):
    if segment == 'femur':
        lig = lig_femur
    elif segment == 'fibula':
        lig = lig_fibula
    else:
        lig = lig_tibia

    if segment == 'femur':
        body = 'femur_distal_l'
    else:
        body = 'tibia_proximal_l'

    if segment == 'femur':
        no_pathpoint = 0
    else:
        no_pathpoint = 1
    for i in range(0, forces.getSize() - 1):
        ligament = opensim.Blankevoort1991Ligament.safeDownCast(forces.get(i))
        if ligament is not None:
            point = opensim.PathPoint.safeDownCast(ligament.upd_GeometryPath().updPathPointSet().get(no_pathpoint))
            if point.getBodyName() == 'tibia_proximal_r':
                osim_points_use = point.getLocation(state).to_numpy()+(0,0.015629,0)-(0,-0.016730,0)
                point.setLocation(opensim.Vec3(osim_points_use))
    # scale ligament slack length based on the length of the ligament compared to the generic model
    # it fails if a point is within a wrapping surface (I think) - in that case run scaling MT parameters in Matlab for now
        lig_names = ["PCLpm1", "PCLpm2", "PCLpm3", "PCLpm4", "PCLpm5", "PCLal1", "PCLal2", "PCLal3", "PCLal4", "PCLal5", "MCLd1",
               "MCLd2", "MCLd3", "MCLd4", "MCLd5", "MCLp1", "MCLp2", "MCLp3", "MCLp4", "MCLp5", "ACLpl1", "ACLpl2",
               "ACLpl3", "ACLpl4", "ACLpl5", "ACLpl6", "ACLam1", "ACLam2", "ACLam3", "ACLam4", "ACLam5", "ACLam6",
               "LCL1", "LCL2", "LCL3", "LCL4"]
        # model_gen = opensim.Model(osim_model)
        # state_gen = model_gen.initSystem()
        # scaleFactor = np.zeros([1, len(lig_names)])
        # for i in range(0, len(lig)):
        #     ligament_gen = opensim.Blankevoort1991Ligament.safeDownCast(model_gen.getForceSet().get(lig[i]))
        #     ligament_pers = opensim.Blankevoort1991Ligament.safeDownCast(model.getForceSet().get(lig[i]))
        #     print(lig[i])
        #     ll_gen = ligament_gen.getLength(state_gen)
        #     sl_gen = ligament_gen.get_slack_length()
        #     ll_pers = ligament_pers.getLength(state)
        #
        #     scaleFactor[0, i] = ll_pers / ll_gen
        #     ligament_pers.set_slack_length(sl_gen * scaleFactor[0, i])

        model.printToXML(out_model)

# out_model2 = r'C:\opensim-jam\opensim-jam-release\examples\walkingS0L\S0L_lig_' + str(sample) + '_contact.osim'
# if add_contact == 1:
#     # add contact to model
#     import xml.etree.ElementTree as ET
#
#     tree = ET.parse(r"C:\opensim-jam\opensim-jam-release\examples\walkingS0L\contact.xml")
#     root = tree.getroot()
#     # Find element to copy
#     # member1 = root.find('Smith2018ArticularContactForce')
#
#     tree2 = ET.parse(out_model)
#     root2 = tree2.getroot()
#     root2.find('Model').find('ForceSet').find('objects').insert(-1, root[0])
#     root2.find('Model').find('ForceSet').find('objects').insert(-1, root[1])
#     root2.find('Model').find('ContactGeometrySet').find('objects').insert(-1, root[2])
#     root2.find('Model').find('ContactGeometrySet').find('objects').insert(-1, root[3])
#     root2.find('Model').find('ContactGeometrySet').find('objects').insert(-1, root[4])
#
#     tree2.write(out_model2)



