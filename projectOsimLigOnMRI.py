import numpy as np
import os
import trimesh
os.environ['PATH'] = r"C:\OpenSim 4.3\bin" + os.pathsep + os.environ['PATH']
import opensim

osim_model = r'C:\opensim-jam\opensim-jam-release\examples\walkingS0L\S0L_lig_nocontact.osim' # r'C:\\opensim-jam\\opensim-jam-release\\examples\\walkingS0_meniscusL\\S0L_meniscus_lig.osim'
osim_model_gen = r'C:\opensim-jam\opensim-jam-release\examples\walkingS0L\S0L_nocontact.osim'

subjects = ['S0']  # [9,13,19,23,26,29,32,35,37,41]
sides = ['L']
segments = ['femur','tibia','fibula']  # 'femur','tibia',
short_ssm = [0, 1, 0]
data_folder = r"C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim"

lig_femur = ["PCLpm1", "PCLpm2", "PCLpm3", "PCLpm4", "PCLpm5", "PCLal1", "PCLal2", "PCLal3", "PCLal4", "PCLal5",
             "MCLs1", "MCLs2", "MCLs3", "MCLs4", "MCLs5", "MCLs6", "MCLd1", "MCLd2", "MCLd3", "MCLd4", "MCLd5", "MCLp1",
             "MCLp2", "MCLp3","MCLp4", "MCLp5", "ACLpl1", "ACLpl2", "ACLpl3", "ACLpl4", "ACLpl5", "ACLpl6", "ACLam1",
             "ACLam2", "ACLam3", "ACLam4", "ACLam5", "ACLam6", "LCL1", "LCL2", "LCL3", "LCL4"]
lig_tibia = ["PCLpm1", "PCLpm2", "PCLpm3", "PCLpm4", "PCLpm5", "PCLal1", "PCLal2", "PCLal3", "PCLal4", "PCLal5",
             "MCLd1", "MCLd2", "MCLd3", "MCLd4", "MCLd5", "MCLp1", "MCLp2", "MCLp3", "MCLp4", "MCLp5", "ACLpl1",
             "ACLpl2", "ACLpl3", "ACLpl4", "ACLpl5", "ACLpl6", "ACLam1", "ACLam2", "ACLam3", "ACLam4", "ACLam5",
             "ACLam6"]
lig_fibula = ["LCL1", "LCL2", "LCL3", "LCL4"]

for subj_ind, subject in enumerate(subjects):

    osim_points = []

    if sides[subj_ind] == 'R':
        side = '_R'
        reflect = ''
    else:
        side = '_L'
        reflect = '.reflect'

    for seg_ind, segment in enumerate(segments):
        if short_ssm[seg_ind]:
            short = '_short'
        else:
            short = ''

        if segment == 'femur':
            lig = lig_femur
        elif segment == 'fibula':
            lig = lig_fibula
        else:
            lig = lig_tibia

        path = data_folder

        # get ligament locations
        if segment == 'femur':
            no_pathpoint = 0
        else:
            no_pathpoint = 1
        # run ICP to get final position SSM point cloud on original mesh
        # mesh OpenSim model -  make sure this is high quality
        mesh1 = trimesh.load_mesh(
             r'C:\\Users\\mariskawesseli\\Documents\\LigamentStudy\\MRI\\S0_prelim\\input_mesh_' + segment + short + '_icp.stl')  # mesh in OS model
        # mesh segmented from MRI
        mesh2 = trimesh.load_mesh(
            r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output/' + segment + '_bone' + short + '/new_bone_mri\input\S0_L_' + segment + '_remesh.STL')  # mesh in position MRI

        # Mirror if needed (only for left as SSM/model is right? - check how to deal with left model)
        M = trimesh.transformations.scale_and_translate((1, -1, 1))
        # mesh2.apply_transform(M)
        # Rotate segmented bone (check why needed?)
        origin, xaxis, yaxis, zaxis = [0, 0, 0], [1, 0, 0], [0, 1, 0], [0, 0, 1]
        Rx = trimesh.transformations.rotation_matrix(-90 / (180 / np.pi), xaxis)
        Ry = trimesh.transformations.rotation_matrix(90 / (180 / np.pi), yaxis)
        # Rz = trimesh.transformations.rotation_matrix(180 / (180 / np.pi), zaxis)
        R = trimesh.transformations.concatenate_matrices(Ry, Rx)
        mesh2.apply_transform(R)
        # Translate segmented mesh to OpenSim bone location
        T = trimesh.transformations.translation_matrix(mesh1.center_mass - mesh2.center_mass)
        mesh2.apply_transform(T)

        # ICP to fit segmented bone to OpenSim mesh
        kwargs = {"scale": False}
        icp1 = trimesh.registration.icp(mesh2.vertices, mesh1, initial=np.identity(4), threshold=1e-5,
                                        max_iterations=20, **kwargs)
        kwargs = {"scale": True}
        icp = trimesh.registration.icp(mesh2.vertices, mesh1, initial=icp1[0], threshold=1e-5, max_iterations=20,
                                       **kwargs)
        mesh2.apply_transform(icp[0])
        scale, shear, angles, trans, persp = trimesh.transformations.decompose_matrix(icp[0])

        # RM = trimesh.transformations.reflection_matrix(mesh2.center_mass, [0, 0, 1])
        # mesh2.apply_transform(RM)
        # mesh2.export(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\bone_fibula_1_tissue_rot_remesh_test_reverse2.STL')

        RM = trimesh.transformations.reflection_matrix(mesh2.center_mass, [1, 0, 0])
        RM2 = trimesh.transformations.reflection_matrix(mesh2.center_mass, [0, 0, 1])
        # mesh4 = trimesh.load_mesh(
        #     r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim/fibula/' + segment + '/' + segment + '.stl')
        mesh3 = trimesh.load_mesh(
            r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\input_mesh_' + segment + short + '_icp.stl')
        # mesh3.apply_transform(np.linalg.inv(RM))
        # mesh3.apply_transform(np.linalg.inv(RM2))
        mesh3.apply_transform(np.linalg.inv(icp[0]))
        mesh3.apply_transform(np.linalg.inv(T))
        mesh3.apply_transform(np.linalg.inv(R))
        # mesh3.apply_transform(np.linalg.inv(M))
        kwargs = {"scale": False}
        # icp3 = trimesh.registration.icp(mesh3.vertices, mesh4, initial=np.identity(4), threshold=1e-5,
        #                                 max_iterations=20, **kwargs)
        # mesh3.apply_transform(icp3[0])
        mesh3.export(
            r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\bone_' + segment + '_1_tissue_rot_remesh_test_reverse.STL')

        for models in range(0,2):
            lig_coord_pers = []
            if segment == 'fibula':
                segment_temp = 'tibia'
            else:
                segment_temp = segment
            if models == 0:
                model_osim = opensim.Model(osim_model)
            elif models == 1:
                model_osim = opensim.Model(osim_model_gen)

            for i in range(0, len(lig)):
                state_osim = model_osim.initSystem()
                ligament_pers = opensim.Blankevoort1991Ligament.safeDownCast(model_osim.getForceSet().get(lig[i]))
                print(lig[i])
                for j in range(0,ligament_pers.get_GeometryPath().getPathPointSet().getSize()):
                    if segment_temp in ligament_pers.get_GeometryPath().getPathPointSet().get(j).getBodyName():
                        lig_coord_pers.append(np.asarray((ligament_pers.get_GeometryPath().getPathPointSet().get(j).getLocation(state_osim)[0]*1000,
                                              ligament_pers.get_GeometryPath().getPathPointSet().get(j).getLocation(state_osim)[1]*1000,
                                              ligament_pers.get_GeometryPath().getPathPointSet().get(j).getLocation(state_osim)[2]*1000)))

            if models == 0:
                name = ''
            elif models == 1:
                name = '_gen'
            np.savetxt(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\fibula\SSM_' + segment + '_pred_points_osim_local' + name + '.xyz',
                    lig_coord_pers, delimiter=" ")

            ligs = trimesh.load_mesh(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\fibula\SSM_' + segment + '_pred_points_osim_local' + name + '.xyz')
            ligs.apply_transform(np.linalg.inv(icp[0]))
            ligs.apply_transform(np.linalg.inv(T))
            ligs.apply_transform(np.linalg.inv(R))
            id = np.arange(0,len(ligs.vertices))

            np.savetxt(
                r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\fibula\SSM_' + segment + '_pred_points_osim_mri' + name + '.fcsv',
                np.c_[id, -ligs.vertices[:,0], -ligs.vertices[:,1], ligs.vertices[:,2]], delimiter=",")

            np.savetxt(
                r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\fibula\SSM_' + segment + '_pred_points_osim_mri' + name + '.xyz',
                np.c_[ligs.vertices], delimiter=" ")

        ligs_all = trimesh.load_mesh(r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\SSM_' + segment + short + '_pred_points_osim.xyz')
        ligs_all.apply_transform(np.linalg.inv(icp[0]))
        ligs_all.apply_transform(np.linalg.inv(T))
        ligs_all.apply_transform(np.linalg.inv(R))
        id = np.arange(0, len(ligs_all.vertices))

        np.savetxt(
            r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\fibula\SSM_' + segment + '_pred_points_osim_mri_all.fcsv',
            np.c_[id, -ligs_all.vertices[:, 0], -ligs_all.vertices[:, 1], ligs_all.vertices[:, 2]], delimiter=",")

        np.savetxt(
            r'C:\Users\mariskawesseli\Documents\LigamentStudy\MRI\S0_prelim\fibula\SSM_' + segment + '_pred_points_osim_mri_all.xyz',
            np.c_[ligs_all.vertices], delimiter=" ")
