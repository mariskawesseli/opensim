import os
import xml.etree.ElementTree as ET
import shutil


def adapt_setup(file, tags, input, tool):
    if tool == 'COMAKInverseKinematicsTool':
        tags_ik = tags[2:-1]
        input_ik = input[2:-1]
        tags = tags[0:2]
        input = input[0:2]
    tree = ET.parse(file)
    root = tree.getroot()
    if tool == 'COMAKInverseKinematicsTool':
        for i, tag in enumerate(tags_ik):
            elem = root.find(tool).find('InverseKinematicsTool').find(tag)
            elem.text = input_ik[i]
    for i, tag in enumerate(tags):
        elem = root.find(tool).find(tag)
        elem.text = input[i]

    tree.write(file)


# create cmd files
input_files = ['comak_inverse_kinematics_settings2_walk6','comak_settings2_walk6','joint_mechanics_settings2_walk6','inverse_dynamics_settings2_walk6']
var_to_change = [['results_directory', 'secondary_constraint_function_file',
                  'results_directory', 'marker_file', 'output_motion_file'],
                 ['coordinates_file', 'results_directory', 'settle_sim_results_directory'],
                 ['states_file', 'results_directory'],
                 ['results_directory', 'coordinates_file']]
tool_names = ['COMAKInverseKinematicsTool','COMAKTool','JointMechanicsTool','InverseDynamicsTool']
for sample in range(0,1):
    main_dir = [r'Y:\MultiscaleModelling\Data\S0\GaitAnalysis\Mariska\OpenSim\12DoF']
    common_dir = ['results_' + str(sample), 'inputs_' + str(sample)]
    new_var = [[os.path.join(main_dir[0], common_dir[0],'comak-inverse-kinematics'), os.path.join(main_dir[0], common_dir[0], 'comak-inverse-kinematics', 'secondary_coordinate_constraint_functions.xml'),
                os.path.join(main_dir[0], common_dir[0],'comak-inverse-kinematics'),os.path.join(main_dir[0],'L.KneeMarker','walk7_LKnee_(' + str(sample) + '_20).trc'),os.path.join(main_dir[0], common_dir[0],'comak-inverse-kinematics','walk6_ik.mot')],
               [os.path.join(main_dir[0], common_dir[0],'comak-inverse-kinematics','walk6_ik.mot'), os.path.join(main_dir[0], common_dir[0],'comak'), os.path.join(main_dir[0], common_dir[0]), 'comak'],
               [os.path.join(main_dir[0], common_dir[0],'comak','walking_states.sto'),os.path.join(main_dir[0], common_dir[0],'joint-mechanics')],
               [os.path.join(main_dir[0], common_dir[0],'inverse-dynamics'),os.path.join(main_dir[0], common_dir[0],'comak','walking_values.sto')]]
    for dir1 in main_dir:
        for dir2 in common_dir:
            try:
                os.makedirs(os.path.join(dir1, dir2))
            except OSError:
                pass
            if dir2 == 'inputs_' + str(sample):
                for ind, file in enumerate(input_files):
                    original = os.path.join(dir1, file + '.xml')
                    target = os.path.join(dir1, dir2, file + '.xml')
                    shutil.copyfile(original, target)

                    adapt_setup(target,var_to_change[ind], new_var[ind], tool_names[ind])

                run_file = open(os.path.join(dir1,'run_walking_walk6_' + str(sample) + '.cmd'), mode='w')

                run_file.writelines(r"set BIN=C:\opensim-jam\opensim-jam-release\bin")
                run_file.writelines("\n")
                run_file.writelines(r"set OPENSIM=C:\opensim-jam\opensim-jam-release\opensim")
                run_file.writelines("\n")
                run_file.writelines(r"set PATH=%BIN%; %OPENSIM%; %PATH%")
                run_file.writelines("\n")
                run_file.writelines(r"%BIN%\comak-inverse-kinematics %BIN%\jam_plugin.dll " + dir1 + "/" + dir2 + "/" + input_files[0] + r".xml")
                run_file.writelines("\n")
                run_file.writelines(r"move out.log " + dir1 + "/" + dir2 + "/" + input_files[0] + r"_out.log")
                run_file.writelines("\n")
                run_file.writelines(r"move err.log " + dir1 + "/" + dir2 + "/" + input_files[0] + r"_err.log")
                run_file.writelines("\n")
                run_file.writelines(r"%BIN%\comak %BIN%\jam_plugin.dll " + dir1 + "/" + dir2 + "/" + input_files[1] + r".xml")
                run_file.writelines("\n")
                run_file.writelines(r"move out.log " + dir1 + "/" + dir2 + "/" + input_files[1] + r"_out.log")
                run_file.writelines("\n")
                run_file.writelines(r"move err.log " + dir1 + "/" + dir2 + "/" + input_files[1] + r"_err.log")
                run_file.writelines("\n")
                run_file.writelines(r"%BIN%\joint-mechanics %BIN%\jam_plugin.dll " + dir1 + "/" + dir2 + "/" + input_files[2] + r".xml")
                run_file.writelines("\n")
                run_file.writelines(r"move out.log " + dir1 + "/" + dir2 + "/" + input_files[2] + r"_out.log")
                run_file.writelines("\n")
                run_file.writelines(r"move err.log " + dir1 + "/" + dir2 + "/" + input_files[2] + r"_err.log")
                run_file.writelines("\n")
                run_file.writelines(r"%BIN%\opensim-cmd -L %BIN%\jam_plugin.dll run-tool " + dir1 + "/" + dir2 + "/" + input_files[3] + r".xml")
                run_file.writelines("\n")
                run_file.writelines(r"move out.log " + dir1 + "/" + dir2 + "/" + input_files[3] + r"_out.log")
                run_file.writelines("\n")
                run_file.writelines(r"move err.log " + dir1 + "/" + dir2 + "/" + input_files[3] + r"_err.log")
                run_file.close()

                cmd_files = open(r"Y:\MultiscaleModelling\Data\S0\GaitAnalysis\Mariska\OpenSim\12DoF\run_walking2_walk6.cmd", mode="a")
                cmd_files.writelines(os.path.join(dir1, 'run_walking_walk6_' + str(sample) + '.cmd'))
                cmd_files.writelines("\n")
                cmd_files.close()
